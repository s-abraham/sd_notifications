﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using System.Configuration;
using BusinessEntities;
using System.Diagnostics;
using System.Data.Objects;
using BusinessLogic;
namespace SOCIALDEALER.EmailNotificationMonthly
{
    class Program
    {
        private static int EmailTypeID = Convert.ToInt32(ConfigurationManager.AppSettings["EmailTypeID"]);
        private static ApplicationError appError = new ApplicationError();
        private static ErrorLog _errorLog = new ErrorLog();
        private static EventLog _logger;
        private static string EventLogSourceName = Convert.ToString(ConfigurationManager.AppSettings["EventLogSourceName"]);

        private static string DayOfWeek = Convert.ToString(ConfigurationManager.AppSettings["DayOfWeek"]);
        private static int MessageTypeID = Convert.ToInt32(ConfigurationManager.AppSettings["MessageTypeID"]);
        private static int DealerGroupID = Convert.ToInt32(ConfigurationManager.AppSettings["DealerGroupID"]);
        static void Main(string[] args)
        {
            string EventLogMessage = string.Empty;

            try
            {

                if (!EventLog.SourceExists(EventLogSourceName))
                {
                    EventLog.CreateEventSource(EventLogSourceName, EventLogSourceName);
                }
                _logger = new EventLog();
                _logger.Source = EventLogSourceName;

                EventLogMessage = string.Format("[EmailNotificationMonthly.Main()] Monthly Email Notification Started at {0}", Convert.ToString(DateTime.Now));
                Debug.WriteLine(EventLogMessage);
                WriteEventLogEntry(EventLogMessage, EventLogEntryType.Information);

                SocialDealerEntities DataContext = new SocialDealerEntities();

                ObjectResult<spGetEmailUserPermission_Result> ObjectResultspGetEmailUserPermission_Result = DataContext.spGetEmailUserPermission(EmailTypeID);

                List<spGetEmailUserPermission_Result> listspGetEmailUserPermission_Result = ObjectResultspGetEmailUserPermission_Result.ToList();

                DateTime fromDate;
                DateTime toDate;

                string _Month = Convert.ToString(ConfigurationManager.AppSettings["Month"]);
                string _Year = Convert.ToString(ConfigurationManager.AppSettings["Year"]);

                if (string.IsNullOrEmpty(_Month) || string.IsNullOrEmpty(_Year))
                {
                    fromDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1);
                    toDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1);
                }
                else
                {
                    fromDate = new DateTime(Convert.ToInt32(_Year), Convert.ToInt32(_Month), 1);
                    toDate = new DateTime(Convert.ToInt32(_Year), Convert.ToInt32(_Month), DateTime.DaysInMonth(Convert.ToInt32(_Year), Convert.ToInt32(_Month)));

                    //fromDate = DateTime.Parse(_Month + "/01/" + _Year);
                    //toDate = fromDate.AddDays(DateTime.DaysInMonth(Convert.ToInt32(_Year), Convert.ToInt32(_Month)) - 1); 
                }

                var result = false;

                EventLogMessage = string.Format("[EmailNotificationMonthly.Main()] Count of UserID to Process : {0}", Convert.ToString(listspGetEmailUserPermission_Result.Count));
                Debug.WriteLine(EventLogMessage);
                WriteEventLogEntry(EventLogMessage, EventLogEntryType.Information);

                foreach (var spGetEmailUserPermission_Result in listspGetEmailUserPermission_Result)
                {
                    try
                    {
                        EventLogMessage = string.Format("[EmailNotificationMonthly.Main()] Processing UserID: {0}, Name : {1}, Email Address : {2} ", Convert.ToString(spGetEmailUserPermission_Result.UserID), spGetEmailUserPermission_Result.Name, spGetEmailUserPermission_Result.Email);
                        Debug.WriteLine(EventLogMessage);
                        WriteEventLogEntry(EventLogMessage, EventLogEntryType.Information);

                        ObjectResult<spProcessEmailNotification_Result1> spProcessEmailNotification_Result = DataContext.spProcessEmailNotification(spGetEmailUserPermission_Result.UserID, fromDate, toDate, DealerGroupID, "month");

                        List<spProcessEmailNotification_Result1> listspProcessEmailNotification_Result = spProcessEmailNotification_Result.ToList();

                        if (listspProcessEmailNotification_Result.Count > 0)
                        {
                            EmailManager ObjEmailManager = new EmailManager();

                            result = ObjEmailManager.ProcessMonthlyEmail(listspProcessEmailNotification_Result, MessageTypeID, fromDate, toDate, spGetEmailUserPermission_Result.Email);

                            if (result == true)
                            {
                                EventLogMessage = string.Format("[EmailNotificationMonthly.Main()] Email Sent Successfully for UserID: {0}, Name : {1}, Email Address : {2}, Dealer Count : {3} ", Convert.ToString(spGetEmailUserPermission_Result.UserID), spGetEmailUserPermission_Result.Name, spGetEmailUserPermission_Result.Email, Convert.ToString(listspProcessEmailNotification_Result.Count));
                                Debug.WriteLine(EventLogMessage);
                                WriteEventLogEntry(EventLogMessage, EventLogEntryType.Information);

                                DataContext.spUpdateEmailUserPermission(spGetEmailUserPermission_Result.ID);
                            }
                            else
                            {
                                EventLogMessage = string.Format("[EmailNotificationMonthly.Main()] Error in Send Email for UserID: {0}, Name : {1}, Email Address : {2}, Dealer Count : {3}", Convert.ToString(spGetEmailUserPermission_Result.UserID), spGetEmailUserPermission_Result.Name, spGetEmailUserPermission_Result.Email, Convert.ToString(listspProcessEmailNotification_Result.Count));
                                Debug.WriteLine(EventLogMessage);
                                WriteEventLogEntry(EventLogMessage, EventLogEntryType.Information);
                            }
                        }
                        //Console.WriteLine("Run Weekly Email");
                    }
                    catch (Exception ex)
                    {
                        #region ErrorHandler
                        appError.ClassName = "EmailNotificationMonthly";
                        appError.MethodName = "Main";
                        appError.Section = "For Loop";
                        appError.ExceptionType = "General";
                        appError.StackSource = ex.StackTrace;
                        appError.DealerLocationId = 0;
                        appError.ExtraInfo = "";
                        appError.Message = ex.Message + ":" + ex.InnerException;
                        WriteEventLogEntry(appError.ToString(), EventLogEntryType.Error);
                        Debug.WriteLine(appError.ToString());
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailNotificationMonthly";
                appError.MethodName = "Main";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                WriteEventLogEntry(appError.ToString(), EventLogEntryType.Error);
                Debug.WriteLine(appError.ToString());
                #endregion
            }


        }

        private static void WriteEventLogEntry(string message, EventLogEntryType type)
        {
            _logger.WriteEntry(message, type);
        }
    }
}
