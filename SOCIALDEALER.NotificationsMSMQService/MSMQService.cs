﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Messaging;
using System.Configuration;
using SOCIALDEALER.NotificationsMSMQService.WinServiceTest;
using ProxyLibrary;
using System.Xml.Serialization;
using System.IO;

namespace SOCIALDEALER.NotificationsMSMQService
{
    public partial class MSMQService : ServiceBase, IDebuggableService
    {
        private string stMSMQPrivateQueue = string.Empty;
        private EventLog _logger;
        private string EventLogMessage = string.Empty;
        private string EventLogSourceName = Convert.ToString(ConfigurationManager.AppSettings["EventLogSourceName"]);
        MessageQueue MyMessageQ = null;

        public MSMQService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _logger = new EventLog { Source = EventLogSourceName };            

            stMSMQPrivateQueue = ConfigurationManager.AppSettings["QueueName"];
            MSMQTimer.Enabled = true;
            //MyMessageQ = new MessageQueue(stMSMQPrivateQueue) { Formatter = new XmlMessageFormatter(new Type[] { typeof(string) }) };
            //MyMessageQ.PeekCompleted += new PeekCompletedEventHandler(QueuePeekCompleted);
            //MyMessageQ.BeginPeek();
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
        }

        //private void QueuePeekCompleted(object sender, PeekCompletedEventArgs e)
        //{
        //    try
        //    {
        //        var message = MyMessageQ.EndPeek(e.AsyncResult);

        //        if (message != null)
        //        {
        //            if (message.Label == "ReviewNotification")
        //            {
        //                Console.WriteLine("\nA message was read from queue.");
        //                Console.WriteLine(string.Format("Message : {0}", message.Body));   
        //            }
        //            message.Dispose();
        //            message = null;
        //        }
                

        //        MyMessageQ.Receive();
        //        MyMessageQ.BeginPeek();
        //    }
        //    catch (Exception ex)
        //    {

        //    }           
        //}

        private void MSMQTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            MSMQTimer.Enabled = false;
            string strReviewNotification = string.Empty;

            svrNotificationMgr.NotificationMgrClient ObjsvrNotificationMgr = new svrNotificationMgr.NotificationMgrClient();
            try
            {
                using (MyMessageQ = new MessageQueue(stMSMQPrivateQueue))
                {
                    MyMessageQ.Formatter = new XmlMessageFormatter(new string[] { "System.String" });
                    Message MyMessage = MyMessageQ.Receive();
                    if (MyMessage != null)
                    {
                        MSMQTimer.Interval = 5000;
                        if (MyMessage.Label == "ReviewNotification")
                        {
                            //Common.Add2Log("AppLog", 5, TraceEventType.Information, "Label = " + MyMessage.Label + " , Body = " + MyMessage.Body.ToString());                            

                            strReviewNotification = MyMessage.Body.ToString();
                            
                            if (!string.IsNullOrEmpty(strReviewNotification))
                            {
                                EventLogMessage = String.Format("[NotificationsMSMQService.MSMQService] MSMQ Reveived, Message ID : {0}", strReviewNotification);                                
                                _logger.WriteEntry(EventLogMessage, EventLogEntryType.Warning);

                                ObjsvrNotificationMgr.CreateNewNotification(Convert.ToInt32(strReviewNotification));

                            }
                        }
                        MyMessage.Dispose();
                        MyMessage = null;
                    }
                    else
                    {
                        MSMQTimer.Interval = 30000;
                    }
                }
                MyMessageQ = null;
            }
            catch (Exception ex)
            {
                EventLogMessage = String.Format("[NotificationsMSMQService.MSMQService] Message ID: {0},  Exception : {1}, InnerException : {2}", strReviewNotification, ex.Message, ex.InnerException);
                _logger.WriteEntry(EventLogMessage, EventLogEntryType.Error);
            }
            MSMQTimer.Enabled = true;
        }

        private T FromXml<T>(String xml)
        {
            T returnedXmlClass = default(T);

            try
            {
                using (TextReader reader = new StringReader(xml))
                {
                    try
                    {
                        returnedXmlClass = (T)new XmlSerializer(typeof(T)).Deserialize(reader);
                    }
                    catch (InvalidOperationException)
                    {
                        // String passed is not XML, simply return defaultXmlClass
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return returnedXmlClass;
        }

        private bool SendReviewtoNotification(string strReviewNotification)
        {
            
            try
            {
                
            }
            catch (Exception ex)
            {

            }

            return true;
        }

        //private bool SendReviewtoNotification(string strReviewNotification)
        //{
        //    ProxyDetails _proxyDetails = new ProxyDetails();
        //    Proxy _proxy = new Proxy();
        //    try
        //    {
        //        _proxyDetails.requestURL = "http://localhost/SOCIALDEALER.NotificationsWS/NotificationMgr.svc/CreateNewNotification";
        //        _proxyDetails.method = "POST";
        //        _proxyDetails.timeout = 22000;
        //        _proxyDetails.UseProxy = false;
        //        _proxyDetails.reTry = false;
        //        _proxyDetails.reTrynum = 0;
        //        _proxyDetails.readResponse = true;
        //        _proxyDetails.xmlrequest = strReviewNotification;
                
        //        _proxy.ExecuteCommand(_proxyDetails);
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //    return true;
        //}
        #region IDebuggableService Members

        public void Start(string[] args)
        {
            OnStart(args);
        }

        public void StopService()
        {
            OnStop();
        }

        public void Pause()
        {
            OnPause();
        }

        public void Continue()
        {
            OnContinue();
        }

        #endregion        
    }
}
