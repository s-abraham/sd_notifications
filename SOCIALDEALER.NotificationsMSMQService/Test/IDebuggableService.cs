﻿using System.ServiceProcess;

namespace SOCIALDEALER.NotificationsMSMQService.WinServiceTest
{
    public interface IDebuggableService
    {
        void Start(string[] args);
        void StopService();
        void Pause();
        void Continue();
    }
}
