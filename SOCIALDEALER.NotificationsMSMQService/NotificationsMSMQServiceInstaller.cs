﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;


namespace SOCIALDEALER.NotificationsMSMQService
{
    [RunInstaller(true)]
    public partial class NotificationsMSMQServiceInstaller : System.Configuration.Install.Installer
    {
        public NotificationsMSMQServiceInstaller()
        {
            InitializeComponent();
        }
    }
}
