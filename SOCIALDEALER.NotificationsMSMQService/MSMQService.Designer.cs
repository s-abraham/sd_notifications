﻿namespace SOCIALDEALER.NotificationsMSMQService
{
    partial class MSMQService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Timers.Timer MSMQTimer;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MSMQTimer = new System.Timers.Timer();
            ((System.ComponentModel.ISupportInitialize)(this.MSMQTimer)).BeginInit();
            // 
            // MSMQTimer
            // 
            this.MSMQTimer.Interval = 30000;
            this.MSMQTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.MSMQTimer_Elapsed);
            // 
            // MSMQService
            // 
            this.ServiceName = "SOCIALDEALER.NotificationsMSMQService";
            ((System.ComponentModel.ISupportInitialize)(this.MSMQTimer)).EndInit();

        }

        #endregion
    }
}
