﻿using System;
using System.Diagnostics;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using BusinessLogic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SOCIALDEALER.Tests
{
    [TestClass]
    public class EmailManagerTests
    {
        private EmailManager emailManager;

        public EmailManagerTests()
        {
            emailManager = new EmailManager();
        }

        //[TestMethod]
        //public void TestMethod1()
        //{
        //    // Method Signature: public void SaveEmailRequest_NewReview(int dealerLocationId, string reviewEntryXML)
        //    var dealerLocationId = 1;
        //    var reviewEntryXML = XDocument.Load("S:\\Queries\\Notifications\\DL Summary Sample.xml").ToString();

        //    var actualResult = emailManager.SaveEmailRequest_NewReview(reviewEntryXML);

        //    Assert.IsTrue(actualResult);

        //}

        [TestMethod]
        public void ProcessPendingNotifications_Tests()
        {
            // Method Signature: public int ProcessPendingNotifications(int dealerLocationId = 0)
            var dealerLocationId = 1;

            var actualResult = emailManager.ProcessPendingNotifications();

            Assert.IsTrue(actualResult > 0);
        }


    }
}
