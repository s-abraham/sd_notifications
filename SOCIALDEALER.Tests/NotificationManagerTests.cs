﻿using System;
using System.Diagnostics;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using BusinessEntities;
using BusinessLogic;
using DataLayer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net;
using System.Net.Mail;
using SendGridMail;
using SendGridMail.Transport;

namespace SOCIALDEALER.Tests
{
    [TestClass]
    public class NotificationManagerTests
    {
        private NotificationManager _notificationManager;

        public NotificationManagerTests()
        {
            _notificationManager = new BusinessLogic.NotificationManager();
        }

        [TestMethod]
        public void Insert()
        {

        }

        //[TestMethod]
        //public void SaveNotificationTest()
        //{
        //    //Method Signature=public static bool SaveNotification(DataLayer.Notification notification)
        //    var sw = Stopwatch.StartNew();

        //    var subject = "Subject From Test";
        //    var body = "Body from Test";

        //    var notification = new Notification
        //    {
        //        MessageTypeId = 1,
        //        Subject = subject,
        //        Body = body,
        //        CreatedDate = DateTime.Today,
        //        DealerLocationId = 1,
        //        ProcessedDate = DateTime.Today,
        //        CustomerId = 1
        //    };

        //    var result = _notificationManager.SaveNotification(notification);
        //    Debug.WriteLine("SaveNotificationTest Processing Time: {0}ms Result = {1}", sw.ElapsedMilliseconds, result);
        //    Assert.IsTrue(result > 0);
        //}

        [TestMethod]
        public void SaveNotificationTransmissionTest()
        {
            //Method Signature=public static bool SaveNotificationTransmission(DataLayer.NotificationTransmission notificationTransmission)
            //var sw = Stopwatch.StartNew();
            //var notificationTransmission = new NotificationTransmission
            //{
            //    ContactDetails = "<ContactDetails><To>sanjua@hotmail.com</To><From>test@yahoo.com</From></ContactDetails>",
            //    NotificationId = 2,
            //    TransmissionTypeId = 1,
            //    DateUpdated = DateTime.Today,
            //    ScheduledDate = DateTime.Today,
            //    StatusId = 1,
            //    ServiceNumber = 1
            //};

            //notificationTransmission.ContactDetails = "";

            //var result = _notificationManager.SaveNotificationTransmission(notificationTransmission);
            //Debug.WriteLine("SaveNotificationTransmission Processing Time: {0}ms Result = {1}", sw.ElapsedMilliseconds, result);
            //Assert.IsTrue(result > 0);
        }

        [TestMethod]
        public void SaveNotificationEntryTest()
        {
            var result = false;

            var messageTypeId = 1;
            var messageCategoryId = 1;
            var subject = "";
            var body = "";
            var dealerLocationId = 1;
            var contactDetails = "<ContactDetails><To>shiva.laughs@gmail.com</To><From>test@yahoo.com</From></ContactDetails>";
            var transmissionTypeId = 1;
            var scheduledDate = DateTime.Now.AddHours(-2);


            var notification = new Notification
                                   {
                                       MessageTypeId = messageTypeId,
                                       MessageCategoryId = messageCategoryId,
                                       Subject = subject,
                                       Body = body,
                                       CreatedDate = DateTime.Today,
                                       DealerLocationId = dealerLocationId,
                                       ProcessedDate = DateTime.Today,
                                       CustomerId = 0
                                   };

            var notificationTransmission = new NotificationTransmission
                                               {
                                                   ContactDetails = contactDetails,
                                                   TransmissionTypeId = transmissionTypeId,
                                                   DateUpdated = DateTime.Today,
                                                   ScheduledDate = scheduledDate,
                                                   StatusId = 1,
                                                   ServiceNumber = 1
                                               };

            var notificationEntry = new NotificationEntry
                                        {
                                            NotificationDetail = notification,
                                            NotificationTransmissionDetail = notificationTransmission
                                        };

            var notifResult = _notificationManager.SaveNotificationEntry(notificationEntry);



        }

        [TestMethod]
        public void GetPendingEmailsTest()
        {
            //Method Signature=public List<spGetNotificationsWorkList_Result> GetPendingEmails(int dealerLocationId = 0, bool pendingOnly = true)
            var sw = Stopwatch.StartNew();

            //var _emailManager = new EmailManager();

            //var results = _emailManager.GetPendingEmails(1);

            Debug.WriteLine("Time Elapsed: {0}", sw.ElapsedMilliseconds);
        }

        [TestMethod]
        public void GetP()
        {
            //Method Signature=public int ProcessPendingNotifications(int dealerLocationId = 0)
            var sw = Stopwatch.StartNew();

            //var _emailManager = new EmailManager();

            //var results = _emailManager.ProcessPendingNotifications(1);

            Debug.WriteLine("Time Elapsed: {0}", sw.ElapsedMilliseconds);
        }

        [TestMethod]
        public void SaveNotificationTest()
        {
            var sw = Stopwatch.StartNew();
            var _username = "socialdealer";
            var _password = "j0rd2n12";

            var message = SendGrid.GenerateInstance();

            message.AddTo("sanjua@hotmail.com");
            message.From = new MailAddress("notifications@socialdealer.com");
            message.Html = " This is a test";
            message.Subject = "This is a test email " + DateTime.Now;

            message.EnableClickTracking();
            message.EnableOpenTracking();
            message.EnableSpamCheck();


            //create an instance of the SMTP transport mechanism
            var transportInstance = SMTP.GenerateInstance(new NetworkCredential(_username, _password));

            //send the mail
            transportInstance.Deliver(message);
            Debug.WriteLine("SaveNotificationTest Processing Time: {0}ms ", sw.ElapsedMilliseconds);
        }


    }
}
