﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using BusinessEntities;
using DataLayer;
using System.Text;

namespace SOCIALDEALER.NotificationsWS
{

    [ServiceContract]
    public interface INotificationMgr
    {
        //[OperationContract]
        //bool AddNotification(DateTime scheduledDate, string contactDetails, string subject, string body, int dealerLocationId, string customerId, string MessageType);

        [OperationContract]
        bool AddNotificationEmail(DateTime scheduledDate, string contactDetails, string subject, string body, int dealerLocationId, string customerId, int messageTypeId, int messageCategoryId);

        /// <summary>
        /// Create new Notification
        /// </summary>
        /// <param name="">xml request</param>
        [OperationContract]
        [WebInvoke(Method = "POST",
                   ResponseFormat = WebMessageFormat.Xml,
                   RequestFormat = WebMessageFormat.Xml,
                   UriTemplate = "CreateNewNotification",
                   BodyStyle = WebMessageBodyStyle.Bare)]
        bool CreateNewNotification(int NewReviewsNotificationID);

        //[OperationContract]
        //bool AddNotificationSMS(DateTime scheduledDate, string ContactDetails, string subject, string body, int dealerLocationId, string customerId);

        //[OperationContract]
        //List<NotificationModel> GetNotificationsByDealerLocationId(int dealerLocationId);

        //[OperationContract]
        //List<NotificationTransmissionModel> GetNotificationTransmissionsByDealerLocationId(int dealerLocationId);

        //[OperationContract]
        //List<NotificationTransmissionModel> GetNotificationTransmissionsByNotificationId(int notificationId);
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
