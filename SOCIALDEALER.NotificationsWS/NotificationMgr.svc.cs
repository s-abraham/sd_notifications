﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel; 
using System.ServiceModel.Web;
using System.Text;
using BusinessLogic;
using BusinessEntities;
using DataLayer;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Configuration;

namespace SOCIALDEALER.NotificationsWS
{
    [ServiceBehavior(AddressFilterMode = AddressFilterMode.Any)]
    public class NotificationMgr : INotificationMgr
    {
        private NotificationManager _notificationManager = new BusinessLogic.NotificationManager();
        private ApplicationError appError = new ApplicationError();
        private EventLog _logger;
        private string EventLogMessage = string.Empty;
        private string EventLogSourceName = Convert.ToString(ConfigurationManager.AppSettings["EventLogSourceName"]);
       

        public NotificationMgr()
        {
            //_logger = new EventLog { Source = EventLogSourceName };
            if (!EventLog.SourceExists(EventLogSourceName))
            {
                EventLog.CreateEventSource(EventLogSourceName, EventLogSourceName);
            }
            _logger = new EventLog();
            _logger.Source = EventLogSourceName;
        }

        //public CompositeType GetDataUsingDataContract(CompositeType composite)
        //{
        //    if (composite == null)
        //    {
        //        throw new ArgumentNullException("composite");
        //    }
        //    if (composite.BoolValue)
        //    {
        //        composite.StringValue += "Suffix";
        //    }
        //    return composite;
        //}

        public bool AddNotification(DateTime scheduledDate, string contactDetails, string subject, string body, int dealerLocationId, string customerId, int messageTypeId, int messageCategoryId, int transmissionTypeId = 1)
        {
            var result = false;

            try
            {
                var notification = new Notification
                    {
                        MessageTypeId = messageTypeId,
                        MessageCategoryId = messageCategoryId, 
                        Subject = subject,
                        Body = body,
                        CreatedDate = DateTime.Today,
                        DealerLocationId = dealerLocationId,
                        ProcessedDate = DateTime.Today,
                        CustomerId = 0
                    };

                var notificationTransmission = new NotificationTransmission
                    {
                        ContactDetails = contactDetails, TransmissionTypeId = transmissionTypeId, DateUpdated = DateTime.Today, ScheduledDate = scheduledDate, StatusId = 1, ServiceNumber = 1
                    };

                var notificationEntry = new NotificationEntry { NotificationDetail = notification, NotificationTransmissionDetail = notificationTransmission };

                var notifResult = _notificationManager.SaveNotificationEntry(notificationEntry);

                if (notifResult > 0)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "NotificationMgr";
                appError.MethodName = "AddNotification";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace; 
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                result = false;

                #endregion
            }
            
            return result;
        }

        public bool AddNotificationEmail(DateTime scheduledDate, string contactDetails, string subject, string body, int dealerLocationId, string customerId, int messageTypeId, int messageCategoryId)
        {
            var result = false;

            try
            {
                var rawResult = AddNotification(scheduledDate, contactDetails, subject, body, dealerLocationId, customerId, messageTypeId, messageCategoryId, 1);

                if (rawResult)
                {
                    result = true;
                    Debug.WriteLine("Email Notification Updated");
                }
                else
                {
                    result = false;
                    Debug.WriteLine("Email Notification Failed to Update");
                }
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "NotificationMgr";
                appError.MethodName = "AddNotificationEmail";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                result = false;

                #endregion
            }
            
            return result;
        }

        public bool CreateNewNotification(int NewReviewsNotificationID)
        {
            try
            {
                
                EventLogMessage = String.Format("[NotificationsWS.CreateNewNotification] Request Reveived, Message ID : {0}", NewReviewsNotificationID.ToString());
                _logger.WriteEntry(EventLogMessage, EventLogEntryType.Warning);

               //Get XML From Database 
               DealerLocationSummary dealerLocationSummary = GetReviewXMLFromDatabase(NewReviewsNotificationID);
                               
               //dealerLocationSummary = FromXml<DealerLocationSummary>(xmlRequest);
               EmailManager ObjEmailManager = new EmailManager();
               ObjEmailManager.SaveEmailRequest_NewReview(dealerLocationSummary, NewReviewsNotificationID);

               EventLogMessage = String.Format("[NotificationsWS.CreateNewNotification] Request processed successfully, Message ID : {0}", NewReviewsNotificationID.ToString());
               _logger.WriteEntry(EventLogMessage, EventLogEntryType.Warning);
            }
            catch (Exception ex)
            {
                EventLogMessage = String.Format("[NotificationsWS.CreateNewNotification] Message ID: {0},  Exception : {1}, InnerException : {2}", NewReviewsNotificationID.ToString(), ex.Message, ex.InnerException);
                _logger.WriteEntry(EventLogMessage, EventLogEntryType.Error);

            }

            return true;
        }

        private DealerLocationSummary GetReviewXMLFromDatabase(int NewReviewsNotificationID)
        {
            DealerLocationSummary dealerLocationSummary = new DealerLocationSummary();
            try
            {
                SDNotificationEntities ObjSDNotificationEntities = new SDNotificationEntities();
                spGetNewReviewsNotification_Result1 ObjspGetorUpdateNewReviewsNotification_Result = ObjSDNotificationEntities.spGetNewReviewsNotification(Convert.ToInt32(NewReviewsNotificationID)).SingleOrDefault();

                if (ObjspGetorUpdateNewReviewsNotification_Result != null)
                {
                    dealerLocationSummary = Common.FromXml<DealerLocationSummary>(ObjspGetorUpdateNewReviewsNotification_Result.ReviewsNotificationXML);
                }
            }
            catch (Exception ex)
            {
                EventLogMessage = String.Format("[NotificationsWS.GetReviewXMLFromDatabase] Message ID: {0},  Exception : {1}, InnerException : {2}", NewReviewsNotificationID.ToString(), ex.Message, ex.InnerException);
                _logger.WriteEntry(EventLogMessage, EventLogEntryType.Error);

            }

            return dealerLocationSummary;
        }

        

        //public bool AddNotificationSMS(DateTime scheduledDate, string contactDetails, string subject, string body, int dealerLocationId, string customerId)
        //{
        //    var result = false;

        //    try
        //    {
        //        var rawResult = AddNotification(scheduledDate, contactDetails, subject, body, dealerLocationId, customerId, "SMS");

        //        if (rawResult)
        //        {
        //            result = true;
        //            Debug.WriteLine("SMS Notification Updated");
        //        }
        //        else
        //        {
        //            result = false;
        //            Debug.WriteLine("SMS Notification Failed to Update");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        #region ErrorHandler
        //        appError.ClassName = "NotificationMgr";
        //        appError.MethodName = "AddNotificationSMS";
        //        appError.Section = "Main";
        //        appError.ExceptionType = "General";
        //        appError.StackSource = ex.StackTrace;
        //        appError.DealerLocationId = 0;
        //        appError.ExtraInfo = "";
        //        appError.Message = ex.Message + ":" + ex.InnerException;
        //        Debug.WriteLine(appError.ToString());
        //        result = false;
        //        #endregion
        //    }

        //    return result;
        //}

        //public List<NotificationModel> GetNotificationsByDealerLocationId(int dealerLocationId)
        //{
        //    var NotificationModelList = new List<NotificationModel>();

        //    NotificationModelList = _notificationManager.GetNotificationsByDealerLocationId(dealerLocationId);
            
        //    return NotificationModelList;
        //}

        //public List<NotificationTransmissionModel> GetNotificationTransmissionsByDealerLocationId(int dealerLocationId)
        //{
        //    var NotificationTransmissionModellList = new List<NotificationTransmissionModel>();

        //    NotificationTransmissionModellList = _notificationManager.GetNotificationTransmissionsByDealerLocationId(dealerLocationId);

        //    return NotificationTransmissionModellList;
        //}

        //public List<NotificationTransmissionModel> GetNotificationTransmissionsByNotificationId(int notificationId)
        //{
        //    var NotificationTransmissionModellList = new List<NotificationTransmissionModel>();

        //    NotificationTransmissionModellList = _notificationManager.GetNotificationTransmissionsByNotificationId(notificationId);

        //    return NotificationTransmissionModellList;
        //}
    }
}
