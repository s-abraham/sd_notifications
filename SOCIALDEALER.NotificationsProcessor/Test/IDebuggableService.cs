﻿using System.ServiceProcess;

namespace SOCIALDEALER.NotificationsProcessor.WinServiceTest
{
    public interface IDebuggableService
    {
        void Start(string[] args);
        void StopService();
        void Pause();
        void Continue();
    }
}
