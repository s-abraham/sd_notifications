﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using BusinessLogic;
using SOCIALDEALER.NotificationsProcessor.WinServiceTest;

namespace SOCIALDEALER.NotificationsProcessor
{
    public partial class SOCIALDEALER : ServiceBase, IDebuggableService
    {
        private double TimerPeriodminutes = 1;
        private static System.Timers.Timer populatetimer;
        private BusinessLogic.EmailManager _emailManager;
        private int executionNumber = 0;
        private string EventMessage = string.Empty;

        public SOCIALDEALER()
        {
            InitializeComponent();
            _emailManager = new EmailManager();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                TimerPeriodminutes = Convert.ToDouble(ConfigurationManager.AppSettings["TimerPeriodminutes"]);
                
                //populatetimer = new System.Timers.Timer(10000);
                //populatetimer.Elapsed += new ElapsedEventHandler(populatetimer_Elapsed);
                //populatetimer.Interval = 60000 * TimerPeriodminutes;
                //populatetimer.Enabled = true;
                EventMessage = "SOCIALDEALER Notifications Processor started....... ";
                EventLog.WriteEntry(EventMessage);
                Debug.WriteLine(EventMessage);
                EventMessage = "SOCIALDEALER Notifications Processor start Ticking";
                EventLog.WriteEntry(EventMessage);
                Debug.WriteLine(EventMessage);

                processrecord();
            }
            catch (Exception ex)
            {
                EventMessage = "SOCIALDEALER Notifications Processor starting error" + ex.Message;
                EventLog.WriteEntry(EventMessage);
                Debug.WriteLine(EventMessage);
            }

        }

        protected override void OnStop()
        {
            EventLog.WriteEntry("SOCIALDEALER Notifications Processor Stoped........");
        }

        void populatetimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                executionNumber++;
                populatetimer.Enabled = false;
                EventLog.WriteEntry("SOCIALDEALER Notifications Processor Tick........ #" + executionNumber);
                Debug.WriteLine("SOCIALDEALER Notifications Processor Tick #" + executionNumber + ":" + DateTime.Now);
                //_emailManager.ProcessPendingNotifications();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                //System.Threading.Thread.Sleep((60000));
                populatetimer.Enabled = true;
            }
            catch (Exception ex)
            {
                EventMessage = ex.Message + ":" + ex.StackTrace;
                Debug.WriteLine(EventMessage);
                EventLog.WriteEntry(EventMessage + Environment.MachineName, EventLogEntryType.Error);
            }
        }

        void processrecord()
        {
            try
            {
                EventLog.WriteEntry("SOCIALDEALER Notifications Processor start Ticking....... ");
                
                EventLog.WriteEntry("SOCIALDEALER Notifications Processoring Tick........");
                _emailManager.ProcessPendingNotifications();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                //System.Threading.Thread.Sleep((60000));
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("Error SOCIALDEALER Notifications Processoring........");
                EventLog.WriteEntry(ex.Message + "\r" + ex.StackTrace + "\r" + Environment.MachineName, EventLogEntryType.Error);
            }
        }

        #region IDebuggableService Members

        public void Start(string[] args)
        {
            OnStart(args);
        }

        public void StopService()
        {
            OnStop();
        }

        public void Pause()
        {
            OnPause();
        }

        public void Continue()
        {
            OnContinue();
        }

        #endregion        

    }
}
