﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using SendGridMail;
using SendGridMail.Transport;

namespace SOCIALDEALER.Notifications.Email
{
    public class SDEmailer
    {
        private bool sendGrid_EnableTestMode = false;

        private string fromEmailAccount = string.Empty;
        private string sendGrid_Username = string.Empty;
        private string sendGrid_Password = string.Empty;
        private ApplicationError appError = new ApplicationError();
        private string ErrorMessage = string.Empty;

        public bool SendMail(EmailDetail emailDetail, out string errorMessage, bool sendImmediately = false)
        {
            var result = false;
            var errorMessageSB = new StringBuilder();
            try
            {
                var myMessage = SendGrid.GenerateInstance();

                foreach (var destinationTo in emailDetail.DestinationToList)
                {
                    if (sendGrid_EnableTestMode)
                    {
                        if (destinationTo.ToLower().Contains("@socialdealer.com"))
                        {
                            myMessage.AddTo(destinationTo);
                        }
                        else
                        {
                            ErrorMessage = String.Format("Skipping Email {0} because it is not a valid Test Email", destinationTo);
                            errorMessageSB.AppendLine(ErrorMessage);
                            Debug.WriteLine(ErrorMessage);
                        }
                    }
                    else
                    {
                        myMessage.AddTo(destinationTo);
                    }
                }

                foreach (var destinationCC in emailDetail.DestinationCCList)
                {
                    if (sendGrid_EnableTestMode)
                    {
                        if (destinationCC.ToLower().Contains("@socialdealer.com"))
                        {
                            myMessage.AddCc(destinationCC);
                        }
                        else
                        {
                            ErrorMessage = String.Format("Skipping Email {0} because it is not a valid Test Email", destinationCC);
                            errorMessageSB.AppendLine(ErrorMessage);
                            Debug.WriteLine(ErrorMessage);
                        }
                    }
                    else
                    {
                        myMessage.AddCc(destinationCC);
                    }
                }

                foreach (var destinationBCC in emailDetail.DestinationBCCList)
                {
                    if (sendGrid_EnableTestMode)
                    {
                        if (destinationBCC.ToLower().Contains("@socialdealer.com"))
                        {
                            myMessage.AddBcc(destinationBCC);
                        }
                        else
                        {
                            ErrorMessage = String.Format("Skipping Email {0} because it is not a valid Test Email", destinationBCC);
                            errorMessageSB.AppendLine(ErrorMessage);
                            Debug.WriteLine(ErrorMessage);
                        }
                    }
                    else
                    {
                        myMessage.AddBcc(destinationBCC);
                    }
                }

                myMessage.From = new MailAddress(fromEmailAccount);
                myMessage.Subject = emailDetail.Subject;

                myMessage.Html = emailDetail.Body;
                myMessage.Headers.Add("X-SMTPAPI", "{\"category\":" + emailDetail.Category + "\"}");

                myMessage.InitializeFilters();

                if (emailDetail.UseClickTracking)
                {
                    myMessage.EnableClickTracking(true);
                }

                if (emailDetail.UseOpenTracking)
                {
                    myMessage.EnableOpenTracking();
                }

                if (emailDetail.UseSpamCheck)
                {
                    myMessage.EnableSpamCheck();
                }

                // Create credentials, specifying your user name and password.
                var credentials = new NetworkCredential(sendGrid_Username, sendGrid_Password);

                // Create an SMTP transport for sending email.
                var transportSMTP = SMTP.GenerateInstance(credentials);

                // Send the email.
                transportSMTP.Deliver(myMessage);

                
                result = true;
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "SendMail";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                errorMessageSB.AppendLine(appError.ToString());

                #endregion
            }

            errorMessage = errorMessageSB.ToString();
            return result;
        }
    }

    [Serializable()]
    public class EmailDetail
    {
        public EmailDetail()
        {
            DestinationToList = new List<string>();
            DestinationCCList = new List<string>();
            DestinationBCCList = new List<string>();
            Subject = string.Empty;
            Body = string.Empty;
            Category = string.Empty;
            UseClickTracking = true;
            UseOpenTracking = true;
            UseSpamCheck = true;
        }

        [System.Xml.Serialization.XmlElement("DestinationToList")]
        public List<string> DestinationToList { get; set; }

        [System.Xml.Serialization.XmlElement("DestinationCCList")]
        public List<string> DestinationCCList { get; set; }

        [System.Xml.Serialization.XmlElement("DestinationBCCList")]
        public List<string> DestinationBCCList { get; set; }

        [System.Xml.Serialization.XmlElement("Subject")]
        public string Subject { get; set; }

        [System.Xml.Serialization.XmlElement("Body")]
        public string Body { get; set; }

        [System.Xml.Serialization.XmlElement("Category")]
        public string Category { get; set; }

        [System.Xml.Serialization.XmlElement("UseClickTracking")]
        public bool UseClickTracking { get; set; }

        [System.Xml.Serialization.XmlElement("UseOpenTracking")]
        public bool UseOpenTracking { get; set; }

        [System.Xml.Serialization.XmlElement("UseSpamCheck")]
        public bool UseSpamCheck { get; set; }

        public override string ToString()
        {
            var result = new StringBuilder();

            result.Append(" To[" + string.Join(",", DestinationToList.ToArray()) + "]");

            result.Append(" Subject: [" + Subject + "]");
            result.Append(" Body: [" + Body + "]");
            result.Append(" Category: [" + Category + "]");
            result.Append(" UseClickTracking: [" + UseClickTracking + "]");
            result.Append(" UseOpenTracking: [" + UseOpenTracking + "]");
            result.Append(" UseSpamCheck: [" + UseSpamCheck + "]");

            //result.Append(" CC[" + string.Join(",", DestinationCCList.ToArray()) + "]");
            //result.Append(" BCC[" + string.Join(",", DestinationBCCList.ToArray()) + "]");

            return result.ToString();
        }
    }

    public class ApplicationError
    {
        public ApplicationError()
        {
            DealerLocationId = 0;
            Message = string.Empty;
            StackSource = string.Empty;
            ClassName = string.Empty;
            MethodName = string.Empty;
            ExtraInfo = string.Empty;
            Section = string.Empty;
            ExceptionType = string.Empty;
        }

        public int DealerLocationId { get; set; }
        public string Message { get; set; }
        public string StackSource { get; set; }
        public string ClassName { get; set; }
        public string MethodName { get; set; }
        public string ExtraInfo { get; set; }
        public string Section { get; set; }
        public string ExceptionType { get; set; }

        public override string ToString()
        {
            var result = new StringBuilder();
            result.Append(" **********************************");
            result.Append(" Exc [" + ClassName);
            result.Append("." + MethodName + "]");
            result.Append(" \nSec:" + Section);
            result.Append(" Type:" + ExceptionType);
            result.Append(" Message:" + Message);
            result.Append(" **********************************");

            return result.ToString();
        }
    }
}
