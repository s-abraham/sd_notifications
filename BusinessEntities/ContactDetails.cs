﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    [Serializable()]
    public class ContactDetails
    {
        public ContactDetails()
        {
            SendTo = new List<string>();
            SendCC = new List<string>();
            SendBCC = new List<string>();
        }

        [System.Xml.Serialization.XmlElement("SendTo")]
        public List<string> SendTo { get; set; }

        [System.Xml.Serialization.XmlElement("SendCC")]
        public List<string> SendCC { get; set; }

        [System.Xml.Serialization.XmlElement("SendBCC")]
        public List<string> SendBCC { get; set; }

    }
}
