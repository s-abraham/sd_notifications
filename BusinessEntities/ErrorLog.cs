﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class ErrorLog
    {
        public ErrorLog()
        {
            ErrorLogId = 0;
            NotificationId = 0;
            NotificationTransmissionId = 0;
            DealerLocationId = 0;
            Message = string.Empty;
            StackSource = string.Empty;
            ClassName = string.Empty;
            MethodName = string.Empty;
            ExtraInfo = string.Empty;
            Section = string.Empty;
            ExceptionType = string.Empty;
        }
        public int ErrorLogId { get; set; }
        public int DealerLocationId { get; set; }
        public int NotificationId { get; set; }
        public int NotificationTransmissionId { get; set; }
        public string Message { get; set; }
        public string StackSource { get; set; }
        public string ClassName { get; set; }
        public string MethodName { get; set; }
        public string ExtraInfo { get; set; }
        public string Section { get; set; }
        public string ExceptionType { get; set; }

        public override string ToString()
        {
            var result = new StringBuilder();

            result.Append(" Exc [" + ClassName);
            result.Append("." + MethodName + "]");
            result.Append(" Type:" + ExceptionType);
            result.Append(" Message:" + Message);

            return result.ToString();
        }
    }
}
