﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class ReviewAlert
    {
        public int DealerLocationId { get; set; }
        public string DealerLocationName { get; set; }
        public int TotalReviewsCount { get; set; }
        public int TotalNewReviewsCount { get; set; }
        public decimal AverageRating { get; set; }
    }
}
