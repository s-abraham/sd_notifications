﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessEntities
{
    public class Enums
    {
        public enum MessageType
        {
            ReputationActivity = 1,
            PublishActivity = 2,
            SocialActivity = 3,
            ApplicationActivity = 4,
            DowntimeActivity = 5
        }

        public enum TransmissionType
        {
            Email = 1,
            SMS = 2,
            ApplicationPopup = 3,
            ApplicationNotif = 4,
        }

        public enum Status
        {
            Pending = 1,
            Success = 2,
            Failure = 3,
            Cancelled = 4,
            Hold = 5
        }
    }
}
