﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace BusinessEntities
{
    [DataContract]
    public class NotificationModel
    {
        public NotificationModel()
        {
            NotificationId = 0;
            MessageTypeId = 0;
            MessageCategoryId = 0;
            Subject = "";
            Body = "";
            DealerLocationId = 0;
            ProcessedDate = null;
        }

        [DataMember(Name = "Notification Id", Order = 1, IsRequired = true)]
        public int NotificationId { get; set; }

        [DataMember(Name = "Message Type Id", Order = 2, IsRequired = true)]
        public int MessageTypeId { get; set; }

        [DataMember(Name = "Message Category Id", Order = 3, IsRequired = true)]
        public int MessageCategoryId { get; set; }

        [DataMember(Name = "Subject", Order = 4, IsRequired = true)]
        public string Subject { get; set; }

        [DataMember(Name = "Body", Order = 5, IsRequired = true)]
        public string Body { get; set; }

        [DataMember(Name = "Location Id", Order = 6, IsRequired = true)]
        public int DealerLocationId { get; set; }

        [DataMember(Name = "Date Processed", Order = 7, IsRequired = true)]
        public DateTime? ProcessedDate { get; set; }
    }

    [DataContract]
    public class NotificationTransmissionModel
    {
        public NotificationTransmissionModel()
        {
            NotificationTransmissionId = 0;
            NotificationId = 0;
            TransmissionType = 0;
            DateUpdated = DateTime.Now;
            ScheduledDate = DateTime.Now;
            StatusId = 0;
            ContactDetails = "";
        }

        [DataMember(Name = "Transmission Id", Order = 1, IsRequired = true)]
        public int NotificationTransmissionId { get; set; }

        [DataMember(Name = "Notification Id", Order = 2, IsRequired = true)]
        public int NotificationId { get; set; }

        [DataMember(Name = "Transmission Type", Order = 3, IsRequired = true)]
        public int TransmissionType { get; set; }

        [DataMember(Name = "Date Updated", Order = 4, IsRequired = true)]
        public DateTime? DateUpdated { get; set; }

        [DataMember(Name = "Scheduled Date", Order = 5, IsRequired = true)]
        public DateTime ScheduledDate { get; set; }

        [DataMember(Name = "Status Id", Order = 6, IsRequired = true)]
        public int StatusId { get; set; }

        [DataMember(Name = "Contact Details", Order = 7, IsRequired = true)]
        public string ContactDetails { get; set; }
     }


}
