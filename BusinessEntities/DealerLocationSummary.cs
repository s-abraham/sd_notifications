﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace BusinessEntities
{
    //[DataContract(Name = "DealerLocationSummary", Namespace = ""), XmlRoot]
    public class DealerLocationSummary
    {
        public DealerLocationSummary()
        {
            DealerLocationId = 0;
            DealerLocationName = string.Empty;
            TotalReviewsCount = 0;
            TotalNewReviewsCount = 0;
            AverageRating = 0;
            ReviewSummary = new List<ReviewSummary>();
            ReviewsSummaryByReviewSource = new ReviewsSummaryByReviewSource();
        }

        [DataMember(Order = 1)]
        public int DealerLocationId { get; set; }
        [DataMember(Order = 2)]
        public string DealerLocationName { get; set; }
        [DataMember(Order = 3)]
        public int TotalReviewsCount { get; set; }
        [DataMember(Order = 4)]
        public int TotalNewReviewsCount { get; set; }
        [DataMember(Order = 5)]
        public decimal AverageRating { get; set; }   
        [DataMember(Order = 6)]
        public int PositiveNewReviewCount { get; set; }
        [DataMember(Order = 7)]
        public int NegativeNewReviewCount { get; set; }
        [DataMember(Order = 8)]
        public ReviewsSummaryByReviewSource ReviewsSummaryByReviewSource { get; set; }
        [DataMember(Order = 9)]
        public bool isVSMMCustomer { get; set; }
        [DataMember(Order = 10)]
        public List<ReviewSummary> ReviewSummary { get; set; }
    }

    public class ReviewsSummaryByReviewSource
    {
        public ReviewsSummaryByReviewSource()
        {
            ReviewSource = new List<ReviewSource>();
        }
        public List<ReviewSource> ReviewSource { get; set; }
    }

    public class ReviewSource
    {
        public ReviewSource()
        {

        }

        [DataMember(Order = 1)]
        public string ReviewsSourceName { get; set; }
        [DataMember(Order = 2)]
        public string URL { get; set; }
        [DataMember(Order = 3)]
        public int NewReviews { get; set; }
        [DataMember(Order = 4)]
        public int ReviewsSourceID { get; set; }
        [DataMember(Order = 5)]
        public int TotalReviews { get; set; }
        [DataMember(Order = 6)]
        public string ImageSource { get; set; }
        [DataMember(Order = 7)]
        public int NotificationsTypeID { get; set; }
        [DataMember(Order = 8)]
        public decimal Rating { get; set; }
        [DataMember(Order = 9)]
        public int IsCertified { get; set; }
    }

    //[DataContract(Name = "ReviewSummary", Namespace = ""), XmlRoot]
    public class ReviewSummary
    {
        public ReviewSummary()
        {
            Reviews = new List<Review>();
            ExtraInfo = new List<KeyValuePair<string, string>>();
            CollectionDate = DateTime.Now;
        }

        [DataMember(Order = 1)]
        public int DealerLocationId { get; set; }
        [DataMember(Order = 2)]
        public int ReviewSourceId { get; set; }
        [DataMember(Order = 3)]
        public int DealerLocationReviewId { get; set; }
        [DataMember(Order = 4)]
        public int ReviewCountCollected { get; set; }
        [DataMember(Order = 5)]
        public int ReviewCountCalculated { get; set; }
        [DataMember(Order = 6)]
        public decimal RatingCollected { get; set; }
        [DataMember(Order = 7)]
        public decimal RatingCalculated { get; set; }
        [DataMember(Order = 8)]
        public DateTime CollectionDate { get; set; }
        [DataMember(Order = 9)]
        public int NewReviews { get; set; }
        [DataMember(Order = 10)]
        public int Execution { get; set; }
        [DataMember(Order = 11)]
        public int RatingsCountCollected { get; set; }
        [DataMember(Order = 12)]
        public int RatingsCountCalculated { get; set; }
        [DataMember(Order = 13)]
        public List<KeyValuePair<string, string>> ExtraInfo { get; set; }
        [DataMember(Order = 14)]
        public List<Review> Reviews { get; set; }
    }

    //[DataContract(Name = "Reviews", Namespace = ""), XmlRoot]
    public class Review
    {
        public Review()
        {
            ExtraInfo = new extraInfo();
            ReviewTextFull = string.Empty;
            ReviewTextPartial = string.Empty;
            ReviewerName = string.Empty;
            FullReviewURL = string.Empty;

        }

        [DataMember(Order = 1)]
        public int DealerLocationId { get; set; }
        [DataMember(Order = 2)]
        public int ReviewSourceId { get; set; }
        [DataMember(Order = 3)]
        public int DealerLocationReviewId { get; set; }
        [DataMember(Order = 4)]
        public decimal Rating { get; set; }
        [DataMember(Order = 5)]
        public DateTime ReviewDate { get; set; }
        [DataMember(Order = 6)]
        public string ReviewTextFull { get; set; }
        [DataMember(Order = 7)]
        public string ReviewTextPartial { get; set; }
        [DataMember(Order = 8)]
        public string ReviewerName { get; set; }
        [DataMember(Order = 9)]
        public string ReviewId { get; set; }
        [DataMember(Order = 10)]
        public int RepliesCount { get; set; }
        [DataMember(Order = 11)]
        public string FullReviewURL { get; set; }
        [DataMember(Order = 12)]
        public extraInfo ExtraInfo { get; set; }
        [DataMember(Order = 13)]
        public string RatingPolarity { get; set; }
        [DataMember(Order = 13)]
        public string ImageSource { get; set; }
        
        

    }

    public class extraInfo
    {
        public extraInfo()
        {
            ExtraInfo = new List<KeyValuePair<string, string>>();
        }
        public List<KeyValuePair<string, string>> ExtraInfo { get; set; }
    }

    [Serializable]
    [DataContract(Name = "KeyValuePairOfStringString", Namespace = ""), XmlRoot]
    public class KeyValuePair<K, V>
    {
        [DataMember(Order = 1, Name = "Key")]
        public K Key { get; set; }
        [DataMember(Order = 2, Name = "Value")]
        public V Value { get; set; }
        public KeyValuePair() { }
        public KeyValuePair(K key, V value)
        {
            this.Key = key;
            this.Value = value;
        }
    }    
 
}
