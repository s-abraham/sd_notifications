﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using BusinessEntities;
using DataLayer;
using System.Net;
using System.Net.Mail;
using SendGridMail;
using SendGridMail.Transport;

namespace BusinessLogic
{
    public class NotificationManager
    {
        private string EventLogMessage = string.Empty;
        private string EventLogSourceName = string.Empty;
        private ApplicationError appError = new ApplicationError();
        ErrorLog _errorLog = new ErrorLog();
        private EventLog _logger;

        public NotificationManager()
        {
            EventLogSourceName = Convert.ToString(ConfigurationManager.AppSettings["EventLogSourceName"]);
        }

        public int SaveNotificationEntry(NotificationEntry notificationEntry)
        {
            var result = 0;
            var notification_id = 0;

            try
            {
                try
                {
                    notification_id = SaveNotification(notificationEntry.NotificationDetail);
                }
                catch (Exception ex)
                {
                    #region ErrorHandler
                    _errorLog.Message = ex.Message;
                    _errorLog.StackSource = ex.StackTrace;
                    _errorLog.ClassName = "NotificationManager";
                    _errorLog.MethodName = "SaveNotificationEntry";
                    _errorLog.Section = "SaveNotification";
                    _errorLog.ExceptionType = "General";
                    _logger.WriteEntry(_errorLog.ToString(), EventLogEntryType.Error);
                    Debug.WriteLine(_errorLog.ToString());
                    //ObjLibrary.SaveReviewErrorLog(_errorLog);
                    #endregion
                }

                if (notification_id > 0)
                {
                    try
                    {
                        notificationEntry.NotificationTransmissionDetail.NotificationId = notification_id;
                        int notificationTransmission_id = SaveNotificationTransmission(notificationEntry.NotificationTransmissionDetail);

                        if (notificationTransmission_id > 0)
                        {
                            result = notificationTransmission_id;
                        }
                        else
                        {
                            throw new Exception("UnabletoSaveNotificationTransmissionId");
                        }

                    }
                    catch (Exception ex)
                    {
                        #region ErrorHandler
                        _errorLog.Message = ex.Message;
                        _errorLog.StackSource = ex.StackTrace;
                        _errorLog.ClassName = "NotificationManager";
                        _errorLog.MethodName = "SaveNotificationEntry";
                        _errorLog.Section = "SaveNotificationTransmission";
                        _errorLog.ExceptionType = "General";
                        _logger.WriteEntry(_errorLog.ToString(), EventLogEntryType.Error);
                        Debug.WriteLine(_errorLog.ToString());
                        //ObjLibrary.SaveReviewErrorLog(_errorLog);
                        #endregion
                    }
                }
                else
                {
                    throw new Exception("UnabletoSaveNotificationId");
                }
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                _errorLog.Message = ex.Message;
                _errorLog.StackSource = ex.StackTrace;
                _errorLog.ClassName = "NotificationManager";
                _errorLog.MethodName = "SaveNotificationEntry";
                _errorLog.ExceptionType = "General";
                _logger.WriteEntry(_errorLog.ToString(), EventLogEntryType.Error);
                Debug.WriteLine(_errorLog.ToString());
                //ObjLibrary.SaveReviewErrorLog(_errorLog);
                #endregion
            }

            return result;
        }

        private int SaveNotification(DataLayer.Notification notification)
        {
            var result = 0;

            try
            {
                var context = new SDNotificationEntities();

                var activityResult = context.spNotificationInsert(
                    notification.MessageTypeId,
                    notification.MessageCategoryId, 
                    notification.Subject, 
                    notification.Body, 
                    notification.CreatedDate, 
                    notification.DealerLocationId,
                    notification.ProcessedDate,
                    notification.CustomerId,
                    notification.DealerGroupId).SingleOrDefault() ?? 0;

                result = Convert.ToInt32(activityResult);
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "NotificationManager";
                appError.MethodName = "SaveNotification";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace; 
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString()); 
                #endregion
            }

            return result;
        }

        private int SaveNotificationTransmission(DataLayer.NotificationTransmission notificationTransmission)
        {
            var result = 0;

            try
            {
                var context = new SDNotificationEntities();

                var activityResult = context.spNotificationTransmissionInsert(
                    notificationTransmission.NotificationId,
                    notificationTransmission.TransmissionTypeId,
                    notificationTransmission.DateUpdated,
                    notificationTransmission.ScheduledDate,
                    notificationTransmission.StatusId,
                    notificationTransmission.ServiceNumber,
                    Common.Trim(notificationTransmission.ContactDetails)
                    ).SingleOrDefault() ?? 0;

                result = Convert.ToInt32(activityResult);
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "NotificationManager";
                appError.MethodName = "SaveNotificationTransmission";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }

            return result;
        }

        public List<NotificationModel> GetNotificationsByDealerLocationId(int dealerLocationId)
        {
            var results = new List<NotificationModel>();

            try
            {
                using (var context = new SDNotificationEntities())
                {
                    results.AddRange(context.spGetNotificationsByDealerLocationId(dealerLocationId).Select(notification => new NotificationModel
                    {
                        NotificationId = notification.NotificationId,
                        MessageTypeId = notification.MessageTypeId,
                        MessageCategoryId = notification.MessageCategoryId,
                        Subject = notification.Subject,
                        Body = notification.Body,
                        DealerLocationId = notification.DealerLocationId,
                        ProcessedDate = notification.ProcessedDate
                    }));
                }
            }
            catch (Exception)
            {
                
                throw;
            }

            return results;
        }

        public List<NotificationTransmissionModel> GetNotificationTransmissionsByDealerLocationId(int dealerLocationId)
        {
            var results = new List<NotificationTransmissionModel>();

            try
            {
                using (var context = new SDNotificationEntities())
                {
                    results.AddRange(context.spGetNotificationTransmissionsByDealerLocationId(dealerLocationId).Select(notificationTransmission => new NotificationTransmissionModel
                    {
                        NotificationTransmissionId = notificationTransmission.NotificationTransmissionId,
                        NotificationId = notificationTransmission.NotificationId,
                        TransmissionType = notificationTransmission.TransmissionTypeId,
                        DateUpdated = notificationTransmission.DateUpdated,
                        ScheduledDate = notificationTransmission.ScheduledDate,
                        StatusId = notificationTransmission.StatusId,
                        ContactDetails = notificationTransmission.ContactDetails
                    }));
                }
            }
            catch (Exception)
            {

                throw;
            }

            return results;
        }

        public List<NotificationTransmissionModel> GetNotificationTransmissionsByNotificationId(int notificationId)
        {
            var results = new List<NotificationTransmissionModel>();

            try
            {
                using (var context = new SDNotificationEntities())
                {
                    results.AddRange(context.spGetNotificationTransmissionsByNotificationId(notificationId).Select(notificationTransmission => new NotificationTransmissionModel
                    {
                        NotificationTransmissionId = notificationTransmission.NotificationTransmissionId,
                        NotificationId = notificationTransmission.NotificationId,
                        TransmissionType = notificationTransmission.TransmissionTypeId,
                        DateUpdated = notificationTransmission.DateUpdated,
                        ScheduledDate = notificationTransmission.ScheduledDate,
                        StatusId = notificationTransmission.StatusId,
                        ContactDetails = notificationTransmission.ContactDetails
                    }));
                }
            }
            catch (Exception)
            {

                throw;
            }

            return results;
        }

        public void ErrorLogInsert(int dealerLocationId, int notificationId, int notificationTransmissionId, DateTime dateInserted, string message, string stackSource, string className, string methodName, string extraInfo, string emailTo)
        {
            try
            {
                var context = new SDNotificationEntities();

                context.spErrorLogInsert(dealerLocationId, notificationId, notificationTransmissionId, dateInserted, message, stackSource, className, methodName, extraInfo, emailTo);
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                #endregion
            }
        }
    }

    
}
