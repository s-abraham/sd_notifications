﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer;
using System.Data.Objects;

namespace BusinessLogic
{
    public class SDReviewManager
    {
        SDReviewEntities dbContext = new SDReviewEntities();

        public List<spGetReviewDetailByDealerLocationIDforNotification_Result> GetDealerLocationSocialNetworkList(int DealerLocationID)
        {
            List<spGetReviewDetailByDealerLocationIDforNotification_Result> listspGetReviewDetailByDealerLocationIDforNotification_Result = new List<spGetReviewDetailByDealerLocationIDforNotification_Result>();
            try
            {
                ObjectResult<spGetReviewDetailByDealerLocationIDforNotification_Result> objspGetReviewDetailByDealerLocationIDforNotification_Result = dbContext.spGetReviewDetailByDealerLocationIDforNotification(DealerLocationID);

                listspGetReviewDetailByDealerLocationIDforNotification_Result = objspGetReviewDetailByDealerLocationIDforNotification_Result.ToList();
            }
            catch (Exception ex)
            {
                #region ERROR HANDLING

           
                #endregion
            }

            return listspGetReviewDetailByDealerLocationIDforNotification_Result;
        }

       

    }
}
