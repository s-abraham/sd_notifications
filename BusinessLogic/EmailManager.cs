﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using BusinessEntities;
using DataLayer;
using SendGridMail;
using SendGridMail.Transport;

namespace BusinessLogic
{
    public class EmailManager
    {

        #region Properties
        private string EventLogMessage = string.Empty;

        private string EventLogSourceName = string.Empty;
        private ApplicationError appError = new ApplicationError();
        ErrorLog _errorLog = new ErrorLog();
        private EventLog _logger;
        private string fromEmailAccount = string.Empty;
        private string sendGrid_Username = string.Empty;
        private string sendGrid_Password = string.Empty;
        private bool sendGrid_UseClickTracking = false;
        private bool sendGrid_UseOpenTracking = false;
        private bool sendGrid_UseSpamCheck = false;
        private bool sendGrid_EnableTestMode = false;
        private string sendGrid_NewReviewAlertCategory = string.Empty;
        private SDNotificationEntities sdNotificationEntities = new SDNotificationEntities();
        private NotificationManager notificationManager = new NotificationManager();
        private List<spGetTemplateByMessageTypeId_Result> templatesList = new List<spGetTemplateByMessageTypeId_Result>();
        private int MaxNewReview_AlertCount_Dealer;
        private int MaxNewReview_AlertCount_ReviewSource;
        private string AlertEmailAccount_TO;
        private string AlertEmailAccount_CC;
        private string AlertEmailAccount_BCC;
        private int MessageTypeId_Positive_Negative_VSMM;
        private int MessageTypeId_Positive_Negative_NONVSMM;

        private bool SendEmailRealTime; 
        #endregion

        public EmailManager()
        {
            sdNotificationEntities.CommandTimeout = 180;

            fromEmailAccount = Convert.ToString(ConfigurationManager.AppSettings["FromEmailAccount"]);
            sendGrid_Username = Convert.ToString(ConfigurationManager.AppSettings["SendGrid_Username"]);
            sendGrid_Password = Convert.ToString(ConfigurationManager.AppSettings["SendGrid_Password"]);

            sendGrid_UseClickTracking = Convert.ToBoolean(ConfigurationManager.AppSettings["SendGrid_UseClickTracking"]);
            sendGrid_UseOpenTracking = Convert.ToBoolean(ConfigurationManager.AppSettings["SendGrid_UseOpenTracking"]);
            sendGrid_UseSpamCheck = Convert.ToBoolean(ConfigurationManager.AppSettings["SendGrid_UseSpamCheck"]);
            sendGrid_EnableTestMode = Convert.ToBoolean(ConfigurationManager.AppSettings["SendGrid_EnableTestMode"]);

            sendGrid_NewReviewAlertCategory = Convert.ToString(ConfigurationManager.AppSettings["SendGrid_NewReviewAlertCategory"]);

            MaxNewReview_AlertCount_Dealer = Convert.ToInt32(ConfigurationManager.AppSettings["MaxNewReview_AlertCount_Dealer"]); ;
            MaxNewReview_AlertCount_ReviewSource = Convert.ToInt32(ConfigurationManager.AppSettings["MaxNewReview_AlertCount_ReviewSource"]); ;
            AlertEmailAccount_TO = Convert.ToString(ConfigurationManager.AppSettings["AlertEmailAccount_TO"]);
            AlertEmailAccount_CC = Convert.ToString(ConfigurationManager.AppSettings["AlertEmailAccount_CC"]);
            AlertEmailAccount_BCC = Convert.ToString(ConfigurationManager.AppSettings["AlertEmailAccount_BCC"]);

            MessageTypeId_Positive_Negative_VSMM = Convert.ToInt32(ConfigurationManager.AppSettings["MessageTypeId_Positive_Negative_VSMM"]);
            MessageTypeId_Positive_Negative_NONVSMM = Convert.ToInt32(ConfigurationManager.AppSettings["MessageTypeId_Positive_Negative_NONVSMM"]);

            SendEmailRealTime = Convert.ToBoolean(ConfigurationManager.AppSettings["SendEmailRealTime"]);
        }

        public bool SendMail(EmailDetail emailDetail)
        {
            var result = false;
            try
            {
                var myMessage = SendGrid.GenerateInstance();

                foreach (var destinationTo in emailDetail.DestinationToList)
                {
                    if (sendGrid_EnableTestMode)
                    {
                        if (destinationTo.ToLower().Contains("@socialdealer.com"))
                        {
                            myMessage.AddTo(destinationTo);
                        }
                        else
                        {
                            Debug.WriteLine("Skipping Email {0} because it is not a valid Test Email", destinationTo);
                        }
                    }
                    else
                    {
                        myMessage.AddTo(destinationTo);
                    }
                }

                foreach (var destinationCC in emailDetail.DestinationCCList)
                {
                    if (sendGrid_EnableTestMode)
                    {
                        if (destinationCC.ToLower().Contains("@socialdealer.com"))
                        {
                            myMessage.AddCc(destinationCC);
                        }
                        else
                        {
                            Debug.WriteLine("Skipping Email {0} because it is not a valid Test Email", destinationCC);
                        }
                    }
                    else
                    {
                        myMessage.AddCc(destinationCC);
                    }
                }

                foreach (var destinationBCC in emailDetail.DestinationBCCList)
                {
                    if (sendGrid_EnableTestMode)
                    {
                        if (destinationBCC.ToLower().Contains("@socialdealer.com"))
                        {
                            myMessage.AddBcc(destinationBCC);
                        }
                        else
                        {
                            Debug.WriteLine("Skipping Email {0} because it is not a valid Test Email", destinationBCC);
                        }
                    }
                    else
                    {
                        myMessage.AddBcc(destinationBCC);
                    }
                }

                myMessage.From = new MailAddress(fromEmailAccount);
                myMessage.Subject = emailDetail.Subject;

                myMessage.Html = emailDetail.Body;
                myMessage.Headers.Add("X-SMTPAPI", "{\"category\":" + emailDetail.Category + "\"}");

                myMessage.InitializeFilters();

                if (emailDetail.UseClickTracking)
                {
                    myMessage.EnableClickTracking(true);
                }

                if (emailDetail.UseOpenTracking)
                {
                    myMessage.EnableOpenTracking();
                }

                if (emailDetail.UseSpamCheck)
                {
                    myMessage.EnableSpamCheck();
                }

                // Create credentials, specifying your user name and password.
                var credentials = new NetworkCredential(sendGrid_Username, sendGrid_Password);

                // Create an SMTP transport for sending email.
                var transportSMTP = SMTP.GenerateInstance(credentials);

                // Send the email.
                transportSMTP.Deliver(myMessage);

                result = true;
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "SendMail";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }

            return result;
        }
        
        public int PendingNotificationsCount()
        {
            var result = 0;
            var context = new SDNotificationEntities();

            try
            {
                result = (from nt in context.NotificationTransmissions
                                    where nt.StatusId == 1
                                    select nt.NotificationId).Count();
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "SendMail";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }

            return result;
        }

        public int ProcessPendingNotifications(int dealerLocationId = 0)
        {
            var result = 0;
            var totalCount = 0;
            var attemptedCount = 0;
            var sendResult = false;
            var emailDetail = new EmailDetail();
            var contactDetails = new ContactDetails();
            
            try
            {
                var pendingEmailNotifcations = GetPendingEmails(dealerLocationId, true);
                totalCount = pendingEmailNotifcations.Count;
                var context = new SDNotificationEntities();
                foreach (var pendingEmailNotifcation in pendingEmailNotifcations)
                {
                    try
                    {
                        attemptedCount++;
                        Debug.WriteLine(pendingEmailNotifcation.NotificationTransmissionId + " : " + pendingEmailNotifcation.ContactDetails);

                        contactDetails = Common.FromXml<ContactDetails>(pendingEmailNotifcation.ContactDetails);
                        
                        emailDetail = new EmailDetail();
                        emailDetail.Body = pendingEmailNotifcation.Body;
                        
                        emailDetail.DestinationBCCList = contactDetails.SendCC;
                        //emailDetail.DestinationCCList = contactDetails.SendCC;
                        //emailDetail.DestinationToList = contactDetails.SendTo;
                        emailDetail.DestinationToList = new List<string> { "notifications@socialdealer.com" };
                        emailDetail.Subject = pendingEmailNotifcation.Subject;
                        emailDetail.UseClickTracking = sendGrid_UseClickTracking;
                        emailDetail.UseOpenTracking = sendGrid_UseOpenTracking;
                        emailDetail.UseSpamCheck = sendGrid_UseSpamCheck;

                        sendResult = SendMail(emailDetail);
                       
                        if (sendResult)
                        {
                            context.spUpdateNotificationTransmission(pendingEmailNotifcation.NotificationTransmissionId, Convert.ToInt32(LOOKUPSSTATUS.SUCCESS));
                            result++;
                        }
                        else
                        {
                            context.spUpdateNotificationTransmission(pendingEmailNotifcation.NotificationTransmissionId, Convert.ToInt32(LOOKUPSSTATUS.FAILURE));
                        }
                        
                    }
                    catch (Exception ex)
                    {
                        context.spUpdateNotificationTransmission(pendingEmailNotifcation.NotificationTransmissionId, Convert.ToInt32(LOOKUPSSTATUS.FAILURE));

                        #region ErrorHandler
                        appError.ClassName = "EmailManager";
                        appError.MethodName = "ProcessPendingNotifications";
                        appError.Section = "Main";
                        appError.ExceptionType = "General";
                        appError.StackSource = ex.StackTrace;
                        appError.DealerLocationId = 0;
                        appError.ExtraInfo = "";
                        appError.Message = ex.Message + ":" + ex.InnerException;
                        Debug.WriteLine(appError.ToString());
                        #endregion
                    }

                }

                Debug.WriteLine("Attempted Count = {0}, Succeeded = {1}", attemptedCount, result);
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "ProcessPendingNotifications";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }

            return result;
        }

        public bool SaveEmailRequest_NewReview(DealerLocationSummary NewNotification, int NewReviewsNotificationID)
        {
            var result = false;

            var dealerLocationId = NewNotification.DealerLocationId;
            bool SendforApprovalFlage = false;
            int MessageTypeId;
            if (NewNotification.isVSMMCustomer)
            {
                MessageTypeId = MessageTypeId_Positive_Negative_VSMM;
            }
            else
            {
                MessageTypeId = MessageTypeId_Positive_Negative_NONVSMM;
            }
            try
            {
                if (dealerLocationId > 0)
                {
                    templatesList = GetTemplates();
                    var emailDestinations = GetDealerEmails(dealerLocationId);

                    if (emailDestinations.Count > 0)                                                 
                    {

                        //Old Code
                        //TODO: Handle MessageType 7 now and handle rest later.
                        ////var messageTemplate = templatesList.SingleOrDefault(x => x.MessageTypeId == 7);

                        //New Code
                        //TODO: Handle MessageType 7 now and handle rest later.
                        var messageTemplate = templatesList.SingleOrDefault(x => x.MessageTypeId == MessageTypeId);

                        if (messageTemplate != null)
                        {
                            //var reviewAlert = ParseAndExtractReviewXML(reviewEntryXML);

                            if (NewNotification.TotalNewReviewsCount > MaxNewReview_AlertCount_Dealer)
                            {
                                SendforApprovalFlage = true;
                            }

                            if (!SendforApprovalFlage)
                            {
                                foreach (ReviewSource ObjReviewSource in NewNotification.ReviewsSummaryByReviewSource.ReviewSource)
                                {
                                    if (ObjReviewSource.NewReviews > MaxNewReview_AlertCount_ReviewSource)
                                    {
                                        SendforApprovalFlage = true;
                                    }
                                }
                            }

                            int lookupsStatus;
                            if (SendforApprovalFlage)
                            {
                                lookupsStatus = Convert.ToInt32(LOOKUPSSTATUS.HOLD);
                            }
                            else
                            {
                                lookupsStatus = Convert.ToInt32(LOOKUPSSTATUS.PENDING);
                            }

                            var contactDetails = GenerateContactDetails(emailDestinations);

                            var notificationEntry = new NotificationEntry();
                            notificationEntry.NotificationDetail.ProcessedDate = DateTime.Now;
                            notificationEntry.NotificationDetail.MessageTypeId = MessageTypeId; // New Review Email from SDNotification.lookups.MessageType
                            notificationEntry.NotificationDetail.CreatedDate = DateTime.Now;
                            notificationEntry.NotificationDetail.CustomerId = 0;
                            notificationEntry.NotificationDetail.DealerLocationId = dealerLocationId;
                            notificationEntry.NotificationDetail.MessageCategoryId = 1; // Reputation Activity from SDNotification.[lookups].[MessageCategory]

                            //Old 
                            ////var body = GenerateEmailBody(messageTemplate, NewNotification);
                            ////notificationEntry.NotificationDetail.Body = body;

                            //New
                            var body = GenerateEmailBody_ReviewText(messageTemplate, NewNotification);
                            notificationEntry.NotificationDetail.Body = body;


                            if (body.Length > 0)
                            {
                                notificationEntry.NotificationDetail.Subject = messageTemplate.MessageSubject + " for " + NewNotification.DealerLocationName;

                                notificationEntry.NotificationTransmissionDetail.TransmissionTypeId = 1; //Email from SDNotification.[lookups].[TransmissionType]
                                notificationEntry.NotificationTransmissionDetail.DateUpdated = DateTime.Now;
                                notificationEntry.NotificationTransmissionDetail.ScheduledDate = DateTime.Now;
                                notificationEntry.NotificationTransmissionDetail.StatusId = lookupsStatus; //Pending from SDNotification.[lookups].[Status]
                                notificationEntry.NotificationTransmissionDetail.ServiceNumber = 1;
                                notificationEntry.NotificationTransmissionDetail.ContactDetails = contactDetails;

                                var rawResult = notificationManager.SaveNotificationEntry(notificationEntry);

                                if (rawResult > 0)
                                {
                                    result = true;

                                    //SaveRealTimeEmail_ReviewText(notificationEntry, messageTemplate, NewNotification);

                                    SDNotificationEntities ObjSDNotificationEntities = new SDNotificationEntities();
                                    ObjSDNotificationEntities.spUpdateNewReviewsNotification(Convert.ToInt32(NewReviewsNotificationID), 1);

                                    if (SendEmailRealTime)
                                    {
                                        string Subject = "New Review Alert for " + NewNotification.DealerLocationName;

                                        if (!SendforApprovalFlage)
                                        {
                                            ProcessPendingNotifications_Temp(contactDetails, body, Subject, rawResult);
                                        }
                                        else
                                        {
                                            ContactDetails ObjcontactDetails = new ContactDetails();
                                            ObjcontactDetails.SendTo.Add(AlertEmailAccount_TO);
                                            ObjcontactDetails.SendCC.Add(AlertEmailAccount_CC);
                                            ObjcontactDetails.SendBCC.Add(AlertEmailAccount_BCC);
                                            contactDetails = Common.SerializeObject(ObjcontactDetails);

                                            ProcessPendingNotifications_Temp(contactDetails, body, Subject, rawResult);
                                        }
                                    }
                                }
                            }//body.Length
                            else
                            {
                                SDNotificationEntities ObjSDNotificationEntities = new SDNotificationEntities();
                                ObjSDNotificationEntities.spUpdateNewReviewsNotification(Convert.ToInt32(NewReviewsNotificationID), 2);
                            }
                        }
                    }
                    else
                    {
                        SDNotificationEntities ObjSDNotificationEntities = new SDNotificationEntities();
                        ObjSDNotificationEntities.spUpdateNewReviewsNotification(Convert.ToInt32(NewReviewsNotificationID), 2);
                    }
                }
                else
                {
                    #region ErrorHandler
                    appError.ClassName = "EmailManager";
                    appError.MethodName = "SaveEmailRequest_NewReview";
                    appError.Section = "Invalid Input Parameters";
                    appError.ExceptionType = "General";
                    appError.StackSource = "";
                    appError.DealerLocationId = 0;
                    //appError.ExtraInfo = "<DealerLocationId>" + dealerLocationId + "</DealerLocationId><ReviewEntryXML>" + reviewEntryXML + "</ReviewEntryXML>";
                    appError.Message = "Invalid Input Parameters";
                    Debug.WriteLine(appError.ToString());
                    #endregion
                }
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "SaveEmailRequest_NewReview";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                //appError.ExtraInfo = "<DealerLocationId>" + dealerLocationId + "</DealerLocationId><ReviewEntryXML>" + reviewEntryXML + "</ReviewEntryXML>";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }
            return result;
        }

        //private bool SaveRealTimeEmail_ReviewText(NotificationEntry notificationEntry, spGetTemplateByMessageTypeId_Result messageTemplate, DealerLocationSummary NewNotification)
        //{
        //    bool result = false;
        //    try
        //    {
        //        var body = GenerateEmailBody_ReviewText(messageTemplate, NewNotification);
        //        notificationEntry.NotificationDetail.Body = body;


        //        result = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        #region ErrorHandler
        //        appError.ClassName = "EmailManager";
        //        appError.MethodName = "SaveRealTimeEmail_ReviewText";
        //        appError.Section = "Main";
        //        appError.ExceptionType = "General";
        //        appError.StackSource = ex.StackTrace;
        //        appError.DealerLocationId = 0;
        //        appError.ExtraInfo = "";
        //        appError.Message = ex.Message + ":" + ex.InnerException;
        //        Debug.WriteLine(appError.ToString());
        //        #endregion

        //        result = false;
        //    }


        //    return result;
        //}

        private string GenerateEmailBody_ReviewText(spGetTemplateByMessageTypeId_Result messageTemplate, DealerLocationSummary NewNotification)
        {
            var result = string.Empty;
            SocialDealerEntities dbContext = new SocialDealerEntities();
            SDReviewManager objSDReviewManager = new SDReviewManager();
            NotificationManager objNotificationManager = new NotificationManager();
            bool isEmailbody = false;

            try
            {
                var messageTemplateMain = messageTemplate.MessageTemplate;

                var PositiveRepeating = string.Empty;
                var NegativeRepeating = string.Empty;
                var NonPositiveNegativeRepeating = string.Empty;
                
                var GoogleRatingBlock = string.Empty;
                var NonGoogleRatingBlock = string.Empty;
                var RepeatingRatingBlock_Temp = string.Empty;


                string Template_Reviews = string.Empty;
                string Review_Breaker = "<br />";


                List<spGetReviewDetailByDealerLocationIDforNotification_Result> listspGetReviewDetailByDealerLocationIDforNotification_Result  = objSDReviewManager.GetDealerLocationSocialNetworkList(NewNotification.DealerLocationId);

                messageTemplateMain = messageTemplateMain.Replace("[$DealerLocationName$]", NewNotification.DealerLocationName);

                int TotalNewReviewsCount = 0;
                int PositiveNewReviewCount = 0;
                int NegativeNewReviewCount = 0;

                foreach (var reviewSummary in NewNotification.ReviewSummary)
                {   
                    foreach (var review in reviewSummary.Reviews)
                    {
                        try
                        {   
                            bool sendEmail = true;
                            string Message = "Not Sending Emial.";                           

                            List<spGetReviewDetailByDealerLocationIDforNotification_Result> listReviewDetail = new List<spGetReviewDetailByDealerLocationIDforNotification_Result>();
                            if (reviewSummary.ReviewSourceId != 1)
                            {
                                listReviewDetail = listspGetReviewDetailByDealerLocationIDforNotification_Result.Where(x => x.ReviewsSourceID == reviewSummary.ReviewSourceId && x.ReviewDate.ToShortDateString() == review.ReviewDate.ToShortDateString()).ToList();
                            }
                            else
                            {
                                listReviewDetail = listspGetReviewDetailByDealerLocationIDforNotification_Result.Where(x => x.ReviewsSourceID == reviewSummary.ReviewSourceId).ToList();
                            }

                            string[] NewReviewWords = new string[10];
                            string _NewReviewText = GetPlainText(review.ReviewTextFull);

                            NewReviewWords = _NewReviewText.Split(' ').Where(o => o.Length > 0).Take(10).ToArray();

                            
                            foreach (var item in listReviewDetail)
                            {
                                if (Convert.ToDateTime(review.ReviewDate.ToShortDateString()) < Convert.ToDateTime(DateTime.Now.AddDays(-30).ToShortDateString()))
                                {
                                    Message = "Review Date < 30 Days";
                                    sendEmail = false;
                                    break;
                                }
                                
                                string _review = GetPlainText(item.Review);
                                string ReviewText = string.Empty;
                                int indexofwords = 0;

                                int iCounter = 0;
                                int matchCounter = 0;

                                foreach (var word in NewReviewWords)
                                {
                                    iCounter++;
                                    // find 1st word in Review Text                
                                    if (indexofwords == 0)
                                    {
                                        ReviewText = _review.Trim();
                                    }
                                    else
                                    {
                                        ReviewText = _review.Substring(indexofwords, _review.Trim().Length - indexofwords);
                                    }


                                    if (ReviewText.Contains(word))
                                    {
                                        // get the index of word
                                        int indexofword = _review.Trim().IndexOf(word) + word.Length;
                                        indexofwords = indexofword;
                                        matchCounter++;
                                    }
                                }

                                if (matchCounter == NewReviewWords.Length)
                                {
                                    //Not Send Email
                                    if (Convert.ToDecimal(Math.Round(item.Rating, 1)) == Convert.ToDecimal(Math.Round(review.Rating, 1)))
                                    {
                                        //Not Send if user also same
                                        if (item.ReviewerName != review.ReviewerName)
                                        {
                                            //send                            
                                            Message = "Review and Rating is Matching But UserName is not Matching";
                                            sendEmail = true;
                                        }
                                        else
                                        {
                                            //not send                            
                                            Message = "Review, Rating, UserName is Matching";
                                            sendEmail = false;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        //Send                        
                                        Message = "Review is Matching but Rating is Not Matching (No Need to Check UserName)";
                                        sendEmail = true;
                                    }
                                }
                                else
                                {
                                    Message = "Review is Not Matching (No Need to Check Rating and UserName)";
                                    sendEmail = true;
                                }

                            }

                            if (Convert.ToDateTime(review.ReviewDate.ToShortDateString()) < Convert.ToDateTime(DateTime.Now.AddDays(-30).ToShortDateString()))
                            {
                                Message = "Review Date < 30 Days";
                                sendEmail = false;                                
                            }

                            if (sendEmail)
                            {   

                                RepeatingRatingBlock_Temp = string.Empty;

                                PositiveRepeating = messageTemplate.PositiveRepeating;
                                NegativeRepeating = messageTemplate.NegativeRepeating;
                                NonPositiveNegativeRepeating = messageTemplate.NonPositiveNegativeRepeating;

                                GoogleRatingBlock = messageTemplate.TemplateRepeatingBlock_ReviewSource;
                                NonGoogleRatingBlock = messageTemplate.TemplateRepeatingBlock_Overview;

                                if (review.RatingPolarity == "1") //Positive Review
                                {
                                    PositiveRepeating = PositiveRepeating.Replace("[$ReviewSourceImagePath$]", review.ImageSource);
                                    string URL = string.Empty;

                                    try
                                    {
                                        URL = dbContext.DealerLocationReviews.Where(d => d.ID == review.DealerLocationReviewId).Select(o => o.URL).SingleOrDefault();
                                    }
                                    catch (Exception ex)
                                    {
                                        URL = string.Empty;
                                    }
                                    PositiveRepeating = PositiveRepeating.Replace("[$URL$]", URL);

                                    //PositiveRepeating = PositiveRepeating.Replace("[$Rating$]", Convert.ToString(review.Rating));
                                    PositiveRepeating = PositiveRepeating.Replace("[$ReviewDate$]", DateTime.Parse(Convert.ToString(review.ReviewDate)).ToShortDateString());
                                    PositiveRepeating = PositiveRepeating.Replace("[$ReviewerName$]", review.ReviewerName);
                                    PositiveRepeating = PositiveRepeating.Replace("[$ReviewTextFull$]", review.ReviewTextFull);
                                    #region Old Code
                                    //if (review.ReviewSourceId == 1)
                                    //{
                                    //    //string ExtraInfo = Convert.ToString(review.ExtraInfo);

                                    //    foreach (var lv1 in review.ExtraInfo.ExtraInfo)
                                    //    {
                                    //        var _GoogleRatingBlock = string.Empty;

                                    //        //Console.WriteLine("Key : " + lv1.key.Replace("_Rating", "") + " , Value : " + lv1.value);
                                    //        _GoogleRatingBlock = GoogleRatingBlock.Replace("[$RatingName$]", Convert.ToString(lv1.Key.Replace("_Rating", "")));
                                    //        _GoogleRatingBlock = _GoogleRatingBlock.Replace("[$Rating$]", Convert.ToString(lv1.Value));

                                    //        RepeatingRatingBlock_Temp = RepeatingRatingBlock_Temp + _GoogleRatingBlock;
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    NonGoogleRatingBlock = NonGoogleRatingBlock.Replace("[$Rating$]", Convert.ToString(review.Rating));
                                    //    NonGoogleRatingBlock = NonGoogleRatingBlock.Replace("[$RatingOutof$]", "5");
                                    //    RepeatingRatingBlock_Temp = NonGoogleRatingBlock;
                                    //}
                                    #endregion

                                    NonGoogleRatingBlock = NonGoogleRatingBlock.Replace("[$Rating$]", Convert.ToString(Math.Round(review.Rating, 1)));
                                    NonGoogleRatingBlock = NonGoogleRatingBlock.Replace("[$RatingOutof$]", "5");
                                    RepeatingRatingBlock_Temp = NonGoogleRatingBlock;

                                    PositiveRepeating = PositiveRepeating.Replace("[$ReviewSourceRating$]", RepeatingRatingBlock_Temp);
                                    PositiveRepeating = PositiveRepeating.Replace("[$ReviewTextFull$]", review.ReviewTextFull);
                                    PositiveRepeating = PositiveRepeating + Review_Breaker;

                                    Template_Reviews = Template_Reviews + PositiveRepeating;

                                    TotalNewReviewsCount++;
                                    PositiveNewReviewCount++;
                                }
                                else if (review.RatingPolarity == "0") //Negative Review
                                {
                                    NegativeRepeating = NegativeRepeating.Replace("[$ReviewSourceImagePath$]", review.ImageSource);
                                    string URL = string.Empty;

                                    try
                                    {
                                        URL = dbContext.DealerLocationReviews.Where(d => d.ID == review.DealerLocationReviewId).Select(o => o.URL).SingleOrDefault();
                                    }
                                    catch (Exception ex)
                                    {
                                        URL = string.Empty;
                                    }
                                    NegativeRepeating = NegativeRepeating.Replace("[$URL$]", URL);

                                    //NegativeRepeating = NegativeRepeating.Replace("[$Rating$]", Convert.ToString(review.Rating));
                                    NegativeRepeating = NegativeRepeating.Replace("[$ReviewDate$]", DateTime.Parse(Convert.ToString(review.ReviewDate)).ToShortDateString());
                                    NegativeRepeating = NegativeRepeating.Replace("[$ReviewerName$]", review.ReviewerName);
                                    NegativeRepeating = NegativeRepeating.Replace("[$ReviewTextFull$]", review.ReviewTextFull);
                                    #region Old Code
                                    //if (review.ReviewSourceId == 1)
                                    //{
                                    //    foreach (var lv1 in review.ExtraInfo.ExtraInfo)
                                    //    {
                                    //        var _GoogleRatingBlock = string.Empty;

                                    //        //Console.WriteLine("Key : " + lv1.key.Replace("_Rating", "") + " , Value : " + lv1.value);
                                    //        _GoogleRatingBlock = GoogleRatingBlock.Replace("[$RatingName$]", Convert.ToString(lv1.Key.Replace("_Rating", "")));
                                    //        _GoogleRatingBlock = _GoogleRatingBlock.Replace("[$Rating$]", Convert.ToString(lv1.Value));

                                    //        RepeatingRatingBlock_Temp = RepeatingRatingBlock_Temp + _GoogleRatingBlock;
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    NonGoogleRatingBlock = NonGoogleRatingBlock.Replace("[$Rating$]", Convert.ToString(review.Rating));
                                    //    NonGoogleRatingBlock = NonGoogleRatingBlock.Replace("[$RatingOutof$]", "5");
                                    //    RepeatingRatingBlock_Temp = NonGoogleRatingBlock;
                                    //}
                                    #endregion

                                    NonGoogleRatingBlock = NonGoogleRatingBlock.Replace("[$Rating$]", Convert.ToString(review.Rating));
                                    NonGoogleRatingBlock = NonGoogleRatingBlock.Replace("[$RatingOutof$]", "5");
                                    RepeatingRatingBlock_Temp = NonGoogleRatingBlock;

                                    NegativeRepeating = NegativeRepeating.Replace("[$ReviewSourceRating$]", RepeatingRatingBlock_Temp);
                                    NegativeRepeating = NegativeRepeating + Review_Breaker;

                                    Template_Reviews = Template_Reviews + NegativeRepeating;

                                    TotalNewReviewsCount++;
                                    NegativeNewReviewCount++;
                                }
                                else if (review.RatingPolarity == null || review.RatingPolarity == "")
                                {
                                    NonPositiveNegativeRepeating = NonPositiveNegativeRepeating.Replace("[$ReviewSourceImagePath$]", review.ImageSource);
                                    string URL = string.Empty;

                                    try
                                    {
                                        URL = dbContext.DealerLocationReviews.Where(d => d.ID == review.DealerLocationReviewId).Select(o => o.URL).SingleOrDefault();
                                    }
                                    catch (Exception ex)
                                    {
                                        URL = string.Empty;
                                    }
                                    NonPositiveNegativeRepeating = NonPositiveNegativeRepeating.Replace("[$URL$]", URL);

                                    //PositiveRepeating = PositiveRepeating.Replace("[$Rating$]", Convert.ToString(review.Rating));
                                    NonPositiveNegativeRepeating = NonPositiveNegativeRepeating.Replace("[$ReviewDate$]", DateTime.Parse(Convert.ToString(review.ReviewDate)).ToShortDateString());
                                    NonPositiveNegativeRepeating = NonPositiveNegativeRepeating.Replace("[$ReviewerName$]", review.ReviewerName);
                                    NonPositiveNegativeRepeating = NonPositiveNegativeRepeating.Replace("[$ReviewTextFull$]", review.ReviewTextFull);
                                    #region Old Code
                                    //if (review.ReviewSourceId == 1)
                                    //{
                                    //    //string ExtraInfo = Convert.ToString(review.ExtraInfo);

                                    //    foreach (var lv1 in review.ExtraInfo.ExtraInfo)
                                    //    {
                                    //        var _GoogleRatingBlock = string.Empty;

                                    //        //Console.WriteLine("Key : " + lv1.key.Replace("_Rating", "") + " , Value : " + lv1.value);
                                    //        _GoogleRatingBlock = GoogleRatingBlock.Replace("[$RatingName$]", Convert.ToString(lv1.Key.Replace("_Rating", "")));
                                    //        _GoogleRatingBlock = _GoogleRatingBlock.Replace("[$Rating$]", Convert.ToString(lv1.Value));

                                    //        RepeatingRatingBlock_Temp = RepeatingRatingBlock_Temp + _GoogleRatingBlock;
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    NonGoogleRatingBlock = NonGoogleRatingBlock.Replace("[$Rating$]", Convert.ToString(review.Rating));
                                    //    NonGoogleRatingBlock = NonGoogleRatingBlock.Replace("[$RatingOutof$]", "5");
                                    //    RepeatingRatingBlock_Temp = NonGoogleRatingBlock;
                                    //}
                                    #endregion

                                    NonGoogleRatingBlock = NonGoogleRatingBlock.Replace("[$Rating$]", Convert.ToString(review.Rating));
                                    NonGoogleRatingBlock = NonGoogleRatingBlock.Replace("[$RatingOutof$]", "5");
                                    RepeatingRatingBlock_Temp = NonGoogleRatingBlock;

                                    NonPositiveNegativeRepeating = NonPositiveNegativeRepeating.Replace("[$ReviewSourceRating$]", RepeatingRatingBlock_Temp);
                                    NonPositiveNegativeRepeating = NonPositiveNegativeRepeating.Replace("[$ReviewTextFull$]", review.ReviewTextFull);
                                    NonPositiveNegativeRepeating = NonPositiveNegativeRepeating + Review_Breaker;

                                    Template_Reviews = Template_Reviews + NonPositiveNegativeRepeating;

                                    TotalNewReviewsCount++;
                                }

                                isEmailbody = true;
                            }
                            else
                            {
                                string str = "ReviewSourceId : " + Convert.ToString(review.ReviewSourceId) + " , Rating : " + Convert.ToString(review.Rating) + " , ReviewerName : " + review.ReviewerName;
                                objNotificationManager.ErrorLogInsert(NewNotification.DealerLocationId, 1, 1, DateTime.Now, Message, str + " : " + review.ReviewTextFull, string.Empty, string.Empty, string.Empty, string.Empty);
                            }

                        }                        
                        catch (Exception ex)
                        {
                            #region ErrorHandler
                            appError.ClassName = "EmailManager";
                            appError.MethodName = "GenerateEmailBody_ReviewText";
                            appError.Section = "Process Positive Negative Review";
                            appError.ExceptionType = "General";
                            appError.StackSource = ex.StackTrace;
                            appError.DealerLocationId = 0;
                            appError.ExtraInfo = "";
                            appError.Message = ex.Message + ":" + ex.InnerException;
                            Debug.WriteLine(appError.ToString());
                            #endregion
                        }
                    }
                }

                messageTemplateMain = messageTemplateMain.Replace("[$NewReviewCount$]", Convert.ToString(TotalNewReviewsCount));
                messageTemplateMain = messageTemplateMain.Replace("[$PositiveNewReviewCount$]", Convert.ToString(PositiveNewReviewCount));
                messageTemplateMain = messageTemplateMain.Replace("[$NegativeNewReviewCount$]", Convert.ToString(NegativeNewReviewCount));

                messageTemplateMain = messageTemplateMain.Replace("[$Review$]", Template_Reviews);
                
                result = messageTemplateMain;
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GenerateEmailBody_ReviewText";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }

            if (isEmailbody)
            {
                return result;
            }
            else
            {
                return "";
            }
            
        }

        private string GetPlainText(string input)
        {
            return input.Trim().Replace("!", " ")
                                .Replace("@", " ").Replace("#", " ").Replace("$", " ")
                                .Replace("%", " ").Replace("^", " ").Replace("&", " ")
                                .Replace("*", " ").Replace("-", " ").Replace(@"\", " ")
                                .Replace("/", " ").Replace("_", " ").Replace("&amp;", " ")
                                .Replace(".", " ");
        }

        public bool ProcessPendingNotifications_Temp(string ContactDetails, string Body, string Subject, int NotificationTransmissionId)
        {
            var result = 0;
            var totalCount = 0;
            var succeededCount = 0;
            var sendResult = false;
            var emailDetail = new EmailDetail();
            var contactDetails = new ContactDetails();
            SDNotificationEntities ObjSDNotificationEntities = new SDNotificationEntities();

            try
            {
                succeededCount++;
                //Debug.WriteLine(pendingEmailNotifcation.NotificationTransmissionId + " : " + pendingEmailNotifcation.ContactDetails);


                contactDetails = Common.FromXml<ContactDetails>(ContactDetails);

                emailDetail = new EmailDetail();
                
                emailDetail.Body = Body;
                emailDetail.Category = "New Review Alert";
                emailDetail.DestinationBCCList = contactDetails.SendCC;
                //emailDetail.DestinationCCList = contactDetails.SendCC;
                //emailDetail.DestinationToList = contactDetails.SendTo;
                emailDetail.DestinationToList = new List<string> { "notifications@socialdealer.com" };
                emailDetail.Subject = Subject;
                emailDetail.UseClickTracking = sendGrid_UseClickTracking;
                emailDetail.UseOpenTracking = sendGrid_UseOpenTracking;
                emailDetail.UseSpamCheck = sendGrid_UseSpamCheck;

                sendResult = SendMail(emailDetail);

                //Update NotificationTransmission Status to Success (2)

                if (sendResult)
                {
                    ObjSDNotificationEntities.spUpdateNotificationTransmission(NotificationTransmissionId, Convert.ToInt32(LOOKUPSSTATUS.SUCCESS));
                }
                else
                {
                    ObjSDNotificationEntities.spUpdateNotificationTransmission(NotificationTransmissionId, Convert.ToInt32(LOOKUPSSTATUS.FAILURE));
                }
            }
            catch (Exception ex)
            {
                ObjSDNotificationEntities.spUpdateNotificationTransmission(NotificationTransmissionId, Convert.ToInt32(LOOKUPSSTATUS.FAILURE));

                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "ProcessPendingNotifications";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }

            return sendResult;
        }
        
        private List<spGetNotificationEmailsByLocationId_Result> GetDealerEmails(int dealerLocationId)
        {
            var results = new List<spGetNotificationEmailsByLocationId_Result>();

            try
            {
                results = sdNotificationEntities.spGetNotificationEmailsByLocationId(dealerLocationId).ToList();
            }
            catch (EntitySqlException ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetDealerEmails";
                appError.Section = "Main";
                appError.ExceptionType = "EntitySqlException";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "<DealerLocationId>" + dealerLocationId + "</DealerLocationId>";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }
            catch (EntityCommandExecutionException ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetDealerEmails";
                appError.Section = "Main";
                appError.ExceptionType = "EntityCommandExecutionException";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "<DealerLocationId>" + dealerLocationId + "</DealerLocationId>";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }
            catch (System.Data.EntityException ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetDealerEmails";
                appError.Section = "Main";
                appError.ExceptionType = "EntityException";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "<DealerLocationId>" + dealerLocationId + "</DealerLocationId>";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }
            catch (System.InvalidOperationException ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetDealerEmails";
                appError.Section = "Main";
                appError.ExceptionType = "InvalidOperationException";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "<DealerLocationId>" + dealerLocationId + "</DealerLocationId>";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetDealerEmails";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "<DealerLocationId>" + dealerLocationId + "</DealerLocationId>";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }

            return results;
        }

        private string GenerateContactDetails(List<spGetNotificationEmailsByLocationId_Result> dealerEmailsList)
        {
            var result = string.Empty;
            var emailCount = 0;
            var contactDetails = new ContactDetails();

            try
            {
                foreach (var dealerEmail in dealerEmailsList)
                {
                    emailCount++;
                    contactDetails.SendCC.Add(dealerEmail.Email);
                    //if (emailCount > 1)
                    //{
                    //    contactDetails.SendCC.Add(dealerEmail.Email);
                    //}
                    //else
                    //{
                    //    contactDetails.SendTo.Add(dealerEmail.Email);
                    //}
                }
                result = Common.SerializeObject(contactDetails);
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetDealerEmails";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }

            return result;
        }
        
        private List<spGetTemplateByMessageTypeId_Result> GetTemplates()
        {
            var results = new List<spGetTemplateByMessageTypeId_Result>();

            try
            {
                results = sdNotificationEntities.spGetTemplates().ToList();
            }
            catch (EntitySqlException ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetTemplates";
                appError.Section = "Main";
                appError.ExceptionType = "EntitySqlException";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }
            catch (EntityCommandExecutionException ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetTemplates";
                appError.Section = "Main";
                appError.ExceptionType = "EntityCommandExecutionException";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }
            catch (System.Data.EntityException ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetTemplates";
                appError.Section = "Main";
                appError.ExceptionType = "EntityException";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }
            catch (System.InvalidOperationException ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetTemplates";
                appError.Section = "Main";
                appError.ExceptionType = "InvalidOperationException";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetTemplates";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }

            return results;
        }

        private spGetTemplateByMessageTypeId_Result GetTemplateByMessageTypeId(int messageTypeId)
        {
            var result = new spGetTemplateByMessageTypeId_Result();

            try
            {
                result = sdNotificationEntities.spGetTemplateByMessageTypeId(messageTypeId).SingleOrDefault();
            }
            catch (EntitySqlException ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetTemplateByMessageTypeId";
                appError.Section = "Main";
                appError.ExceptionType = "EntitySqlException";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "<MessageTypeId>" + messageTypeId + "</MessageTypeId>";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }
            catch (EntityCommandExecutionException ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetTemplateByMessageTypeId";
                appError.Section = "Main";
                appError.ExceptionType = "EntityCommandExecutionException";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "<MessageTypeId>" + messageTypeId + "</MessageTypeId>";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }
            catch (System.Data.EntityException ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetTemplateByMessageTypeId";
                appError.Section = "Main";
                appError.ExceptionType = "EntityException";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "<MessageTypeId>" + messageTypeId + "</MessageTypeId>";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }
            catch (System.InvalidOperationException ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetTemplateByMessageTypeId";
                appError.Section = "Main";
                appError.ExceptionType = "InvalidOperationException";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "<MessageTypeId>" + messageTypeId + "</MessageTypeId>";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetTemplateByMessageTypeId";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "<MessageTypeId>" + messageTypeId + "</MessageTypeId>";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }

            return result;
        }

        private ReviewAlert ParseAndExtractReviewXML(string xml)
        {
            var result = new ReviewAlert();

            try
            {
                var xmlDoc = XDocument.Load(xml);

                var info = from item in xmlDoc.Descendants("SummaryInfo")
                           select new ReviewAlert()
                           {
                               DealerLocationId = Convert.ToInt32(item.Element("DealerLocationId").Value ?? "0"),
                               DealerLocationName = item.Element("DealerLocationName").Value ?? string.Empty,
                               TotalReviewsCount = Convert.ToInt32(item.Element("TotalReviewsCount").Value ?? "0"),
                               TotalNewReviewsCount = Convert.ToInt32(item.Element("TotalNewReviewsCount").Value ?? "0"),
                               AverageRating = Convert.ToDecimal(item.Element("AverageRating").Value ?? "0.0")
                           };
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "ParseAndExtractReviewXML";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }

            return result;
        }

        private string GenerateEmailBody(spGetTemplateByMessageTypeId_Result messageTemplate,DealerLocationSummary NewNotification)
        {
            var result = string.Empty;

            try
            {
                var templateRepeatingBlock = messageTemplate.TemplateRepeatingBlock;
                var templateRepeatingBlock_Overview = messageTemplate.TemplateRepeatingBlock_Overview;
                string templateRepeatingBlock_ReviewSource = string.Empty;
                
                var template = messageTemplate.MessageTemplate;

                //Dealership Name (12 New Review)
                templateRepeatingBlock = templateRepeatingBlock.Replace("[$DealerLocationName$]", NewNotification.DealerLocationName);
                templateRepeatingBlock = templateRepeatingBlock.Replace("[$TotalNewReviewsCount$]",Convert.ToString(NewNotification.TotalNewReviewsCount));

                //Overview
                templateRepeatingBlock_Overview = templateRepeatingBlock_Overview.Replace("[$AverageRating$]", Convert.ToString(NewNotification.AverageRating));
                templateRepeatingBlock_Overview = templateRepeatingBlock_Overview.Replace("[$TotalReviewsCount$]", Convert.ToString(NewNotification.TotalReviewsCount));
                templateRepeatingBlock_Overview = templateRepeatingBlock_Overview.Replace("[$TotalNewReviewsCount$]", Convert.ToString(NewNotification.TotalNewReviewsCount));

                string templateRepeatingBlock_ReviewSource_temp = string.Empty;
                //Review Source
                foreach (var reviewSource in NewNotification.ReviewsSummaryByReviewSource.ReviewSource)
                {
                    templateRepeatingBlock_ReviewSource = messageTemplate.TemplateRepeatingBlock_ReviewSource;

                    templateRepeatingBlock_ReviewSource = templateRepeatingBlock_ReviewSource.Replace("[$ReviewSourceImagePath$]", Convert.ToString(reviewSource.ImageSource));
                    templateRepeatingBlock_ReviewSource = templateRepeatingBlock_ReviewSource.Replace("[$ReviewSourceName$]", Convert.ToString(reviewSource.ReviewsSourceName));

                    if (reviewSource.Rating > 0)
                    {
                        templateRepeatingBlock_ReviewSource = templateRepeatingBlock_ReviewSource.Replace("[$Rating$]", Convert.ToString(reviewSource.Rating));
                    }
                    else
                    {
                        templateRepeatingBlock_ReviewSource = templateRepeatingBlock_ReviewSource.Replace("[$Rating$]", "0");
                    }

                    templateRepeatingBlock_ReviewSource = templateRepeatingBlock_ReviewSource.Replace("[$ReviewsCount$]", reviewSource.TotalReviews > 0 ? Convert.ToString(reviewSource.TotalReviews) : "-");
                    templateRepeatingBlock_ReviewSource = templateRepeatingBlock_ReviewSource.Replace("[$NewReviewsCount$]", reviewSource.NewReviews > 0 ? Convert.ToString(reviewSource.NewReviews) : "-");
                    templateRepeatingBlock_ReviewSource = templateRepeatingBlock_ReviewSource.Replace("[$ReviewsLink$]", reviewSource.URL != string.Empty ? reviewSource.URL : "#");
                    

                    templateRepeatingBlock_ReviewSource_temp += templateRepeatingBlock_ReviewSource;
                }

                result = template.Replace("[$TemplateRepeatingBlock$]", templateRepeatingBlock)
                                 .Replace("[$TemplateRepeatingBlock_Overview$]", templateRepeatingBlock_Overview)
                                 .Replace("[$TemplateRepeatingBlock_ReviewSource$]", templateRepeatingBlock_ReviewSource_temp);
                
                
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GenerateEmailBody";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }

            return result;
        }

        private List<spGetNotificationsWorkList_Result> GetPendingEmails(int dealerLocationId = 0, bool pendingOnly = true)
        {
            var results = new List<spGetNotificationsWorkList_Result>();
            var notif = new spGetNotificationsWorkList_Result();
            try
            {
                using (var context = new SDNotificationEntities())
                {
                    IQueryable<NotificationTransmission> worklist = context.NotificationTransmissions;

                    if (dealerLocationId > 0)
                    {
                        worklist = pendingOnly ? worklist.Where(nt => nt.Notification.DealerLocationId == dealerLocationId && ((LOOKUPSSTATUS)nt.StatusId == LOOKUPSSTATUS.PENDING || (LOOKUPSSTATUS)nt.StatusId == LOOKUPSSTATUS.HOLD)) : worklist.Where(nt => nt.Notification.DealerLocationId == dealerLocationId && nt.ScheduledDate <= DateTime.Now);
                    }
                    else
                    {
                        worklist = pendingOnly ? worklist.Where(nt => ((LOOKUPSSTATUS)nt.StatusId == LOOKUPSSTATUS.PENDING || (LOOKUPSSTATUS)nt.StatusId == LOOKUPSSTATUS.HOLD) && nt.ScheduledDate <= DateTime.Now) : worklist.Where(nt => nt.Notification.DealerLocationId == dealerLocationId && nt.ScheduledDate <= DateTime.Now);
                    }

                    var resultsRaw = (from nt in worklist
                                      join n in context.Notifications on nt.NotificationId equals n.NotificationId
                                      where nt.TransmissionTypeId == 1 && nt.ScheduledDate <= DateTime.Now
                                      select new
                                      {
                                          NotificationId = nt.NotificationId,
                                          ContactDetails = (string)nt.ContactDetails,
                                          MessageTypeId = n.MessageTypeId,
                                          MessageCategoryId = n.MessageCategoryId,
                                          NotificationTransmissionId = nt.NotificationTransmissionId,
                                          Subject = n.Subject,
                                          Body = n.Body,
                                          CreatedDate = n.CreatedDate,
                                          DealerLocationId = n.DealerLocationId,
                                          ProcessedDate = n.ProcessedDate,
                                          CustomerId = n.CustomerId,
                                          ScheduledDate = nt.ScheduledDate,
                                          StatusId = nt.StatusId
                                      }).ToList();
                    //int i = 0;
                    foreach (var item in resultsRaw)
                    {
                        
                        notif = new spGetNotificationsWorkList_Result();
                        notif.NotificationId = item.NotificationId;
                        if ((LOOKUPSSTATUS)item.StatusId == LOOKUPSSTATUS.HOLD)
                        {
                            ContactDetails ObjcontactDetails = new ContactDetails();
                            ObjcontactDetails.SendTo.Add(AlertEmailAccount_TO);
                            ObjcontactDetails.SendCC.Add(AlertEmailAccount_CC);
                            ObjcontactDetails.SendBCC.Add(AlertEmailAccount_BCC);
                            string result = Common.SerializeObject(ObjcontactDetails);
                            notif.ContactDetails = result;
                        }
                        else if ((LOOKUPSSTATUS)item.StatusId == LOOKUPSSTATUS.PENDING)
                        {
                            notif.ContactDetails = item.ContactDetails;
                        }
                        notif.MessageTypeId = item.MessageTypeId;
                        notif.MessageCategoryId = item.MessageCategoryId;
                        notif.NotificationTransmissionId = item.NotificationTransmissionId;
                        notif.Subject = item.Subject;
                        notif.Body = item.Body;
                        notif.CreatedDate = item.CreatedDate;
                        notif.DealerLocationId = item.DealerLocationId;
                        notif.ProcessedDate = item.ProcessedDate;
                        notif.CustomerId = item.CustomerId;
                        notif.ScheduledDate = item.ScheduledDate;
                        results.Add(notif);
                        
                    }
                }

                if (results.Count <= 0)
                {
                    #region ErrorHandler
                    appError.ClassName = "EmailManager";
                    appError.MethodName = "GetPendingEmails";
                    appError.Section = "";
                    appError.ExceptionType = "None";
                    appError.StackSource = "";
                    appError.DealerLocationId = dealerLocationId;
                    appError.ExtraInfo = "";
                    appError.Message = "No NotificationTransmissions Found";
                    Debug.WriteLine(appError.ToString());
                    #endregion
                }

            }
            catch (EntitySqlException ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetPendingEmails";
                appError.Section = "Main";
                appError.ExceptionType = "EntitySqlException";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }
            catch (EntityCommandExecutionException ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetPendingEmails";
                appError.Section = "Main";
                appError.ExceptionType = "EntityCommandExecutionException";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }
            catch (System.Data.EntityException ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetPendingEmails";
                appError.Section = "Main";
                appError.ExceptionType = "EntityException";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }
            catch (System.InvalidOperationException ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetPendingEmails";
                appError.Section = "Main";
                appError.ExceptionType = "InvalidOperationException";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "GetPendingEmails";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }
            return results;
        }

        public int SetSavenotificationEntry(string body, string Subject, int DealerGroupID, string contactDetails, int MessageTypeID, int dealerLocationId = 0)
        {
            int rawResult = 0;
            try
            {
                var notificationEntry = new NotificationEntry();

                notificationEntry.NotificationDetail.ProcessedDate = DateTime.Now;
                notificationEntry.NotificationDetail.MessageTypeId = MessageTypeID; // New Review Email from SDNotification.lookups.MessageType
                notificationEntry.NotificationDetail.CreatedDate = DateTime.Now;
                notificationEntry.NotificationDetail.CustomerId = 0;
                notificationEntry.NotificationDetail.DealerLocationId = dealerLocationId;
                notificationEntry.NotificationDetail.DealerGroupId = DealerGroupID;
                notificationEntry.NotificationDetail.MessageCategoryId = 1; // Reputation Activity from SDNotification.[lookups].[MessageCategory]

                notificationEntry.NotificationDetail.Body = body;

                notificationEntry.NotificationDetail.Subject = Subject;

                notificationEntry.NotificationTransmissionDetail.TransmissionTypeId = 1; //Email from SDNotification.[lookups].[TransmissionType]
                notificationEntry.NotificationTransmissionDetail.DateUpdated = DateTime.Now;
                notificationEntry.NotificationTransmissionDetail.ScheduledDate = DateTime.Now;
                notificationEntry.NotificationTransmissionDetail.StatusId = Convert.ToInt32(LOOKUPSSTATUS.PENDING); //Pending from SDNotification.[lookups].[Status]
                notificationEntry.NotificationTransmissionDetail.ServiceNumber = 1;
                notificationEntry.NotificationTransmissionDetail.ContactDetails = contactDetails;


                rawResult = notificationManager.SaveNotificationEntry(notificationEntry);
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "SetSavenotificationEntry";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }

            return rawResult;
        }

        public bool ProcessDailyEmail(List<spProcessEmailNotification_Result1> listspProcessEmailNotification_Result, int MessageTypeID, DateTime fromDate, DateTime toDate, string ToEmailAddress)
        {
            var sendResult = false;
            string body = string.Empty;
            string Subject = string.Empty;
            try
            {
                templatesList = GetTemplates();
                var messageTemplate = templatesList.SingleOrDefault(x => x.MessageTypeId == MessageTypeID);

                var messageTemplateMain = messageTemplate.MessageTemplate;

                var templateRepeatingBlock = string.Empty;
                string templateRepeatingBlock_temp = string.Empty;

                var TotalGroupReviews = (from l in listspProcessEmailNotification_Result
                                         select l.TotalReviews).Sum();

                var AvgGroupRating = listspProcessEmailNotification_Result.Average(r => r.Rating);

                AvgGroupRating = Math.Round(Convert.ToDouble(AvgGroupRating), 1);

                var NewGroupReviews = (from l in listspProcessEmailNotification_Result
                                       select l.TotalNewReviews).Sum();

                var PositiveGroupNewReviews = (from l in listspProcessEmailNotification_Result
                                               select l.TotalPositiveReviews).Sum();

                var NegativeGroupNewReviews = (from l in listspProcessEmailNotification_Result
                                               select l.TotalNegativeReviews).Sum();

                var DealerGroupName = (from l in listspProcessEmailNotification_Result
                                       select l.DealerGroupName).FirstOrDefault();

                var DealerGroupID = (from l in listspProcessEmailNotification_Result
                                        select l.DealerGroupID).FirstOrDefault();

                messageTemplateMain = messageTemplateMain.Replace("[$FromDate$]", Convert.ToString(fromDate.ToShortDateString()));                
                messageTemplateMain = messageTemplateMain.Replace("[$DealerGroupName$]", Convert.ToString(DealerGroupName));

                messageTemplateMain = messageTemplateMain.Replace("[$TotalGroupReviews$]", Convert.ToString(TotalGroupReviews));
                messageTemplateMain = messageTemplateMain.Replace("[$AvgGroupRating$]", Convert.ToString(AvgGroupRating));
                messageTemplateMain = messageTemplateMain.Replace("[$NewGroupReviews$]", Convert.ToString(NewGroupReviews));
                messageTemplateMain = messageTemplateMain.Replace("[$PositiveGroupNewReviews$]", Convert.ToString(PositiveGroupNewReviews));
                messageTemplateMain = messageTemplateMain.Replace("[$NegativeGroupNewReviews$]", Convert.ToString(NegativeGroupNewReviews));



                foreach (var spProcessEmailNotification_Result in listspProcessEmailNotification_Result)
                {
                    templateRepeatingBlock = messageTemplate.TemplateRepeatingBlock;

                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$DealerName$]", Convert.ToString(spProcessEmailNotification_Result.DealerName));
                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$TotalReviews$]", Convert.ToString(spProcessEmailNotification_Result.TotalReviews));
                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$AvgRating$]", Convert.ToString(spProcessEmailNotification_Result.Rating));
                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$NewReviews$]", Convert.ToString(spProcessEmailNotification_Result.TotalNewReviews));
                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$PositiveNewReviews$]", Convert.ToString(spProcessEmailNotification_Result.TotalPositiveReviews));
                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$NegativeNewReviews$]", Convert.ToString(spProcessEmailNotification_Result.TotalNegativeReviews));

                    templateRepeatingBlock_temp = templateRepeatingBlock_temp + templateRepeatingBlock;
                }

                messageTemplateMain = messageTemplateMain.Replace("[$DealerReputationSummary$]", Convert.ToString(templateRepeatingBlock_temp));
                body = messageTemplateMain;

                ContactDetails ObjcontactDetails = new ContactDetails();
                ObjcontactDetails.SendCC.Add(ToEmailAddress);
                string contactDetails = Common.SerializeObject(ObjcontactDetails);

                Subject = messageTemplate.MessageSubject + " " + fromDate.ToShortDateString();

                var rawResult = SetSavenotificationEntry(body, Subject, DealerGroupID, contactDetails, MessageTypeID);

                sendResult = ProcessPendingNotifications_Temp(contactDetails, body, Subject, rawResult);
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "ProcessWeeklyEmail";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }

            return sendResult;
        }

        public bool ProcessWeeklyEmail(List<spProcessEmailNotification_Result1> listspProcessEmailNotification_Result, int MessageTypeID, DateTime fromDate, DateTime toDate,string ToEmailAddress)
        {
            var sendResult = false;
            string body = string.Empty;
            string Subject = string.Empty;
            try
            {
                templatesList = GetTemplates();
                var messageTemplate = templatesList.SingleOrDefault(x => x.MessageTypeId == MessageTypeID);

                var messageTemplateMain = messageTemplate.MessageTemplate;

                var templateRepeatingBlock = string.Empty;
                string templateRepeatingBlock_temp = string.Empty;

                var TotalGroupReviews = String.Format("{0:n0}", (from l in listspProcessEmailNotification_Result
                            select l.TotalReviews).Sum());

                var AvgGroupRating = listspProcessEmailNotification_Result.Average(r => r.Rating);

                AvgGroupRating = Math.Round(Convert.ToDouble(AvgGroupRating), 1);

                var NewGroupReviews = String.Format("{0:n0}", (from l in listspProcessEmailNotification_Result
                                       select l.TotalNewReviews).Sum());

                var PositiveGroupNewReviews = String.Format("{0:n0}", (from l in listspProcessEmailNotification_Result
                                         select l.TotalPositiveReviews).Sum());

                var NegativeGroupNewReviews = String.Format("{0:n0}", (from l in listspProcessEmailNotification_Result
                                               select l.TotalNegativeReviews).Sum());

                var DealerGroupName = (from l in listspProcessEmailNotification_Result
                                               select l.DealerGroupName).FirstOrDefault();

                var DealerGroupID = (from l in listspProcessEmailNotification_Result
                                     select l.DealerGroupID).FirstOrDefault();

                messageTemplateMain = messageTemplateMain.Replace("[$FromDate$]", Convert.ToString(fromDate.ToShortDateString()));
                messageTemplateMain = messageTemplateMain.Replace("[$ToDate$]", Convert.ToString(toDate.ToShortDateString()));
                messageTemplateMain = messageTemplateMain.Replace("[$DealerGroupName$]", Convert.ToString(DealerGroupName));

                messageTemplateMain = messageTemplateMain.Replace("[$TotalGroupReviews$]", TotalGroupReviews);
                messageTemplateMain = messageTemplateMain.Replace("[$AvgGroupRating$]", Convert.ToString(AvgGroupRating));
                messageTemplateMain = messageTemplateMain.Replace("[$NewGroupReviews$]", NewGroupReviews);
                messageTemplateMain = messageTemplateMain.Replace("[$PositiveGroupNewReviews$]", PositiveGroupNewReviews);
                messageTemplateMain = messageTemplateMain.Replace("[$NegativeGroupNewReviews$]",NegativeGroupNewReviews);



                foreach (var spProcessEmailNotification_Result in listspProcessEmailNotification_Result)
                {
                    templateRepeatingBlock = messageTemplate.TemplateRepeatingBlock;

                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$DealerName$]", Convert.ToString(spProcessEmailNotification_Result.DealerName));
                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$TotalReviews$]", Convert.ToString(spProcessEmailNotification_Result.TotalReviews));
                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$AvgRating$]", Convert.ToString(spProcessEmailNotification_Result.Rating));
                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$NewReviews$]", Convert.ToString(spProcessEmailNotification_Result.TotalNewReviews));
                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$PositiveNewReviews$]", Convert.ToString(spProcessEmailNotification_Result.TotalPositiveReviews));
                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$NegativeNewReviews$]", Convert.ToString(spProcessEmailNotification_Result.TotalNegativeReviews));

                    templateRepeatingBlock_temp = templateRepeatingBlock_temp + templateRepeatingBlock;
                }

                messageTemplateMain = messageTemplateMain.Replace("[$DealerReputationSummary$]", Convert.ToString(templateRepeatingBlock_temp));
                body = messageTemplateMain;

                ContactDetails ObjcontactDetails = new ContactDetails();
                ObjcontactDetails.SendCC.Add(ToEmailAddress);
                string contactDetails = Common.SerializeObject(ObjcontactDetails);

                Subject = messageTemplate.MessageSubject + " " + fromDate.ToShortDateString() + " - " + toDate.ToShortDateString();

                var rawResult = SetSavenotificationEntry(body, Subject, DealerGroupID, contactDetails, MessageTypeID);

                sendResult = ProcessPendingNotifications_Temp(contactDetails, body, Subject, rawResult);

            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "ProcessWeeklyEmail";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }

            return sendResult;
        }

        public bool ProcessMonthlyEmail(List<spProcessEmailNotification_Result1> listspProcessEmailNotification_Result, int MessageTypeID, DateTime fromDate, DateTime toDate, string ToEmailAddress)
        {
            var sendResult = false;
            string body = string.Empty;
            string Subject = string.Empty;
            try
            {
                templatesList = GetTemplates();
                var messageTemplate = templatesList.SingleOrDefault(x => x.MessageTypeId == MessageTypeID);

                var messageTemplateMain = messageTemplate.MessageTemplate;

                var templateRepeatingBlock = string.Empty;
                string templateRepeatingBlock_temp = string.Empty;

                var TotalGroupReviews = (from l in listspProcessEmailNotification_Result
                                         select l.TotalReviews).Sum();

                var AvgGroupRating = listspProcessEmailNotification_Result.Average(r => r.Rating);

                var NewGroupReviews = (from l in listspProcessEmailNotification_Result
                                       select l.TotalNewReviews).Sum();

                var PositiveGroupNewReviews = (from l in listspProcessEmailNotification_Result
                                               select l.TotalPositiveReviews).Sum();

                var NegativeGroupNewReviews = (from l in listspProcessEmailNotification_Result
                                               select l.TotalNegativeReviews).Sum();

                var DealerGroupName = (from l in listspProcessEmailNotification_Result
                                       select l.DealerGroupName).FirstOrDefault();

                var DealerGroupID = (from l in listspProcessEmailNotification_Result
                                     select l.DealerGroupID).FirstOrDefault();

                messageTemplateMain = messageTemplateMain.Replace("[$FromDate$]", Convert.ToString(fromDate.ToShortDateString()));
                messageTemplateMain = messageTemplateMain.Replace("[$ToDate$]", Convert.ToString(toDate.ToShortDateString()));
                messageTemplateMain = messageTemplateMain.Replace("[$DealerGroupName$]", Convert.ToString(DealerGroupName));

                messageTemplateMain = messageTemplateMain.Replace("[$TotalGroupReviews$]", Convert.ToString(TotalGroupReviews));
                messageTemplateMain = messageTemplateMain.Replace("[$AvgGroupRating$]", Convert.ToString(AvgGroupRating));
                messageTemplateMain = messageTemplateMain.Replace("[$NewGroupReviews$]", Convert.ToString(NewGroupReviews));
                messageTemplateMain = messageTemplateMain.Replace("[$PositiveGroupNewReviews$]", Convert.ToString(PositiveGroupNewReviews));
                messageTemplateMain = messageTemplateMain.Replace("[$NegativeGroupNewReviews$]", Convert.ToString(NegativeGroupNewReviews));



                foreach (var spProcessEmailNotification_Result in listspProcessEmailNotification_Result)
                {
                    templateRepeatingBlock = messageTemplate.TemplateRepeatingBlock;

                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$DealerName$]", Convert.ToString(spProcessEmailNotification_Result.DealerName));
                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$TotalReviews$]", Convert.ToString(spProcessEmailNotification_Result.TotalReviews));
                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$AvgRating$]", Convert.ToString(spProcessEmailNotification_Result.Rating));
                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$NewReviews$]", Convert.ToString(spProcessEmailNotification_Result.TotalNewReviews));
                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$PositiveNewReviews$]", Convert.ToString(spProcessEmailNotification_Result.TotalPositiveReviews));
                    templateRepeatingBlock = templateRepeatingBlock.Replace("[$NegativeNewReviews$]", Convert.ToString(spProcessEmailNotification_Result.TotalNegativeReviews));

                    templateRepeatingBlock_temp = templateRepeatingBlock_temp + templateRepeatingBlock;
                }

                messageTemplateMain = messageTemplateMain.Replace("[$DealerReputationSummary$]", Convert.ToString(templateRepeatingBlock_temp));
                body = messageTemplateMain;

                ContactDetails ObjcontactDetails = new ContactDetails();
                ObjcontactDetails.SendCC.Add(ToEmailAddress);
                string contactDetails = Common.SerializeObject(ObjcontactDetails);

                Subject = messageTemplate.MessageSubject + " " + fromDate.ToShortDateString() + " - " + toDate.ToShortDateString();

                var rawResult = SetSavenotificationEntry(body, Subject, DealerGroupID, contactDetails, MessageTypeID);

                sendResult = ProcessPendingNotifications_Temp(contactDetails, body, Subject, rawResult);

            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailManager";
                appError.MethodName = "ProcessWeeklyEmail";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                Debug.WriteLine(appError.ToString());
                #endregion
            }

            return sendResult;
        }

    }

    public enum LOOKUPSSTATUS
    {        
        PENDING = 1,
        SUCCESS = 2,
        FAILURE = 3,
        CANCELLED = 4,
        HOLD = 5
    }
}
