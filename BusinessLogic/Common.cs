﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace BusinessLogic
{
    public static class Common
    {
        public static string Trim(string xml)
        {
            //xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
            //<?xml version="1.0" encoding="utf-8"?>
            //xmlns:xsd="http://www.w3.org/2001/XMLSchema"

            string[] trimCommentandNamespaces = new string[] { "<?xml version=\"1.0\" encoding=\"utf-8\"?>", " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", " xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"" };
            foreach (string s in trimCommentandNamespaces)
            {
                xml = xml.Replace(s, "");
            }

            return xml;
        }

        public static T FromXml<T>(String xml)
        {
            T returnedXmlClass = default(T);

            try
            {
                using (TextReader reader = new StringReader(xml))
                {
                    try
                    {
                        returnedXmlClass = (T)new XmlSerializer(typeof(T)).Deserialize(reader);
                    }
                    catch (InvalidOperationException)
                    {
                        // String passed is not XML, simply return defaultXmlClass
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return returnedXmlClass;
        }

        public static string SerializeObject<T>(T obj)
        {
            XmlSerializer xmls = new XmlSerializer(typeof(T));
            using (MemoryStream ms = new MemoryStream())
            {
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();

                using (XmlWriter writer = new XmlTextWriter(ms, new UTF8Encoding()))
                {
                    xmls.Serialize(writer, obj, ns);
                }
                return Encoding.UTF8.GetString(ms.ToArray());
            }
        }
    }
}
