﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects;
using System.Diagnostics;
using System.Linq;
using System.Text;
using BusinessEntities;
using BusinessLogic;
using DataLayer;

namespace SOCIALDEALER.EmailNotificationHourly
{
    class Program
    {
        private static ApplicationError appError = new ApplicationError();
        private static ErrorLog _errorLog = new ErrorLog();
        
        private static string eventLogMessage = string.Empty;
        private static BusinessLogic.EmailManager _emailManager = new EmailManager();
        private static string EventLogSourceName = Convert.ToString(ConfigurationManager.AppSettings["EventLogSourceName"]);

        static void Main(string[] args)
        {
            var pendingNotificationsCount = 0;
            var processedNotificationsCount = 0;
            try
            {
                if (!EventLog.SourceExists(EventLogSourceName))
                {
                    EventLog.CreateEventSource(EventLogSourceName, EventLogSourceName);
                }

                eventLogMessage = string.Format("[EmailNotificationHourly.Main()] Daily Periodic Notification Triggered at {0}", Convert.ToString(DateTime.Now));
                Debug.WriteLine(eventLogMessage);
                WriteEventLogEntry(eventLogMessage, EventLogEntryType.Information);

                pendingNotificationsCount = _emailManager.PendingNotificationsCount();

                eventLogMessage = string.Format("[EmailNotificationHourly.Main()] Found {0} records to process", pendingNotificationsCount);
                Debug.WriteLine(eventLogMessage);
                WriteEventLogEntry(eventLogMessage, EventLogEntryType.Information);

                processedNotificationsCount = ProcessRecord();

                eventLogMessage = string.Format("[EmailNotificationHourly.Main()] Finished Processing {0} records.", processedNotificationsCount);
                Debug.WriteLine(eventLogMessage);
                WriteEventLogEntry(eventLogMessage, EventLogEntryType.Information);
                
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailNotificationHourly";
                appError.MethodName = "Main";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                WriteEventLogEntry(appError.ToString(), EventLogEntryType.Error);
                Debug.WriteLine(appError.ToString());
                #endregion
            }
        }

        private static int ProcessRecord()
        {
            var result = 0;

            try
            {
                result = _emailManager.ProcessPendingNotifications();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            catch (Exception ex)
            {
                #region ErrorHandler
                appError.ClassName = "EmailNotificationHourly";
                appError.MethodName = "ProcessRecord";
                appError.Section = "Main";
                appError.ExceptionType = "General";
                appError.StackSource = ex.StackTrace;
                appError.DealerLocationId = 0;
                appError.ExtraInfo = "";
                appError.Message = ex.Message + ":" + ex.InnerException;
                WriteEventLogEntry(appError.ToString(), EventLogEntryType.Error);
                Debug.WriteLine(appError.ToString());
                #endregion
            }

            return result;
        }

        private static void WriteEventLogEntry(string message, EventLogEntryType type)
        {
            EventLog.WriteEntry(EventLogSourceName, message, type);
        }
    }
}
